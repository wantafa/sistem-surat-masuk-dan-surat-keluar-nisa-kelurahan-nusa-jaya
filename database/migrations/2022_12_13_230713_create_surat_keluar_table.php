<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuratKeluarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_keluar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jenis_surat_id');
            $table->integer('penduduk_id')->unsigned()->nullable();
            $table->date('tanggal');
            $table->string('no_surat', 20);
            $table->string('lampiran', 3);
            $table->string('perihal', 50);
            $table->string('verification', 10);
            $table->string('status', 20);
            $table->string('nama_lurah', 50);
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_keluar');
    }
}
