<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkPindahRumahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sk_pindah_rumah', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('surat_keluar_id')->unsigned()->nullable();
            $table->string('keterangan');
            $table->string('no_kartu_keluarga');
            $table->string('alamat_pindah');
            $table->string('jumlah_pengikut');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sk_pindah_rumah');
    }
}
