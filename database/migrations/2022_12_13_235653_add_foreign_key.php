<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('sk_skck', function (Blueprint $table) {
            $table->foreign('surat_keluar_id')
                ->references('id')
                ->on('surat_keluar')
                ->onDelete('cascade');
        });
        
        
        Schema::table('sk_pindah_rumah', function (Blueprint $table) {

            $table->foreign('surat_keluar_id')
                ->references('id')
                ->on('surat_keluar')
                ->onDelete('cascade');
        });

        Schema::table('sk_belum_punya_rumah', function (Blueprint $table) {

            $table->foreign('surat_keluar_id')
                ->references('id')
                ->on('surat_keluar')
                ->onDelete('cascade');
        });

        Schema::table('sk_surat_nikah', function (Blueprint $table) {

            $table->foreign('surat_keluar_id')
                ->references('id')
                ->on('surat_keluar')
                ->onDelete('cascade');
        });

        Schema::table('sk_nikah_n1', function (Blueprint $table) {
            $table->foreign('penduduk_id_calon')
                ->references('id')
                ->on('penduduk')
                ->onDelete('cascade');
            
            $table->foreign('penduduk_id_ayah')
                ->references('id')
                ->on('penduduk')
                ->onDelete('cascade');
            
            $table->foreign('penduduk_id_ibu')
                ->references('id')
                ->on('penduduk')
                ->onDelete('cascade');

            $table->foreign('surat_nikah_id')
                ->references('id')
                ->on('sk_surat_nikah')
                ->onDelete('cascade');
        });

        Schema::table('sk_nikah_n4', function (Blueprint $table) {
            $table->foreign('penduduk_id_calon_suami')
                ->references('id')
                ->on('penduduk')
                ->onDelete('cascade');
            
                $table->foreign('penduduk_id_calon_istri')
                ->references('id')
                ->on('penduduk')
                ->onDelete('cascade');

            $table->foreign('surat_nikah_id')
                ->references('id')
                ->on('sk_surat_nikah')
                ->onDelete('cascade');
        });
        
        Schema::table('sk_nikah_n5', function (Blueprint $table) {
            $table->foreign('penduduk_id_calon_suami')
                ->references('id')
                ->on('penduduk')
                ->onDelete('cascade');
            
            $table->foreign('penduduk_id_calon_istri')
                ->references('id')
                ->on('penduduk')
                ->onDelete('cascade');
            
            $table->foreign('penduduk_id_ayah_calon')
                ->references('id')
                ->on('penduduk')
                ->onDelete('cascade');

            $table->foreign('penduduk_id_ibu_calon')
                ->references('id')
                ->on('penduduk')
                ->onDelete('cascade');

            $table->foreign('surat_nikah_id')
                ->references('id')
                ->on('sk_surat_nikah')
                ->onDelete('cascade');
        });

        Schema::table('surat_keluar', function (Blueprint $table) {
            $table->foreign('penduduk_id')
                ->references('id')
                ->on('penduduk')
                ->onDelete('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sk_skck', function (Blueprint $table) {
            $table->dropForeign(['penduduk_id', 'surat_keluar_id']);
        });
        
        Schema::table('surat_keluar', function (Blueprint $table) {
            $table->dropForeign(['penduduk_id']);
        });
        
        Schema::table('sk_pindah_rumah', function (Blueprint $table) {
            $table->dropForeign(['penduduk_id', 'surat_keluar_id']);
        });
        
        Schema::table('sk_belum_punya_rumah', function (Blueprint $table) {
            $table->dropForeign(['penduduk_id', 'surat_keluar_id']);
        });

        Schema::table('sk_surat_nikah', function (Blueprint $table) {
            $table->dropForeign(['penduduk_id', 'surat_keluar_id']);
        });

        Schema::table('sk_nikah_n1', function (Blueprint $table) {
            $table->dropForeign(['penduduk_id_calon', 'penduduk_id_ayah', 'penduduk_id_ibu', 'surat_nikah_id']);
        });

        Schema::table('sk_nikah_n4', function (Blueprint $table) {
            $table->dropForeign(['penduduk_id_calon_suami', 'penduduk_id_calon_istri', 'surat_nikah_id']);
        });
        
        Schema::table('sk_nikah_n5', function (Blueprint $table) {
            $table->dropForeign(['penduduk_id_calon_suami', 'penduduk_id_calon_istri', 'penduduk_id_ayah_calon', 'penduduk_id_ibu_calon', 'surat_nikah_id']);
        });

    }
}
