<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkNikahN1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sk_nikah_n1', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('penduduk_id_calon')->unsigned()->nullable();
            $table->integer('penduduk_id_ayah')->unsigned()->nullable();
            $table->integer('penduduk_id_ibu')->unsigned()->nullable();
            $table->integer('surat_nikah_id')->unsigned()->nullable();
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sk_nikah_n1');
    }
}
