<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('nisa1'),
        ]);
        
        DB::table('role')->insert([
            [
                'role_name' => 'admin',
            ],
            [
                'role_name' => 'pelayanan',
            ],
            [
                'role_name' => 'sekretaris lurah',
            ],
            [
                'role_name' => 'lurah',
            ],

        ]);

    }
}
