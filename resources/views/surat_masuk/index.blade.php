@extends ('layouts.base')
@section('title', 'Surat Masuk')
@section('content')

  {{-- content start --}}
  <div class="page-header">
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Surat Masuk</a>
        </li>
    </ul>
</div>
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h4 class="card-title">Surat Masuk</h4>
                    <button class="btn btn-primary btn-round ml-auto add-btn" data-toggle="modal" data-target="#modal_surat_masuk">
                        <i class="fa fa-plus"></i>
                        Tambah
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-datatable" class="table table-striped custom-table w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Nomor Surat</th>
                                <th>Jenis Surat</th>
                                <th>Foto</th>
                                <th>Perihal</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- content end --}}

{{-- modal start --}}

{{-- Surat Masuk Modal start --}}

<div id="modal_surat_masuk" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>Tambah</span> Surat Masuk</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('surat_masuk.store') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="id" name="id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Tanggal<span class="text-danger">*</span></label>
                                <input class="form-control" id="tanggal" name="tanggal" type="date" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Nomor Surat<span class="text-danger">*</span></label>
                                <input class="form-control" id="no_surat" name="no_surat" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Jenis Surat<span class="text-danger">*</span></label>
                                <select name="jenis_surat_id" id="jenis_surat_id" class="form-control">
                                    <option>- Pilih -</option>
                                    @foreach ($jenis_surat as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_surat }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Foto<span class="text-danger">*</span></label>
                                <input class="form-control" id="image" name="image" type="file">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Perihal<span class="text-danger">*</span></label>
                                <input class="form-control" id="perihal" name="perihal" type="text" required>
                            </div>
                        </div>
                    </div>

                    <h4>Disposisi</h4>
                    <div class="form-group">
                        <label class="col-form-label">Asal<span class="text-danger">*</span></label>
                        <input class="form-control" id="asal" name="asal" type="text" required>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Intruksi<span class="text-danger">*</span></label>
                        <input class="form-control" id="intruksi" name="intruksi" type="text" required>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Diteruskan<span class="text-danger">*</span></label>
                        <input class="form-control" id="diteruskan" name="diteruskan" type="text" required>
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">Tanggal Penyelesaian<span class="text-danger">*</span></label>
                        <input class="form-control" id="tgl_penyelesaian" name="tgl_penyelesaian" type="date" required>
                    </div>

                    <div class="modal-footer no-bd">
                        <button class="btn btn-primary submit-btn">Simpan</button>
                        <button class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Surat Masuk Modal end --}}

{{--  Surat Masuk Detail Modal start --}}
<div id="modal_surat_masuk_detail" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>Detail</span> Surat Masuk</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped table-border">
                            <tbody>
                                <tr>
                                    <td>Tanggal :</td>
                                    <td class="text-right" id="tanggal"></td>
                                </tr>
                                <tr>
                                    <td>Nomor Surat :</td>
                                    <td class="text-right" id="no_surat"></td>
                                </tr>
                                <tr>
                                    <td>Jenis Surat :</td>
                                    <td class="text-right" id="jenis_surat"></td>
                                </tr>
                                <tr>
                                    <td>Foto :</td>
                                    <td class="text-right"><img id="image" height="100"></td>
                                </tr>
                                <tr>
                                    <td>Perihal :</td>
                                    <td class="text-right" id="perihal"></td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-striped table-border">
                            <tbody>
                                <h4>Disposisi</h4>
                                <tr>
                                    <td>Asal :</td>
                                    <td class="text-right" id="asal"></td>
                                </tr>
                                <tr>
                                    <td>Intruksi :</td>
                                    <td class="text-right" id="intruksi"></td>
                                </tr>
                                <tr>
                                    <td>Diteruskan :</td>
                                    <td class="text-right" id="diteruskan"></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Penyelesaian :</td>
                                    <td class="text-right" id="tgl_penyelesaian"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--  Surat Masuk Detail Modal end --}}

{{-- Delete Surat Masuk Modal start --}}

<div class="modal custom-modal fade" id="delete_surat_masuk" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLongTitle">Hapus</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <h4 class="text-center">Anda yakin ingin hapus?</h4>
                <div class="modal-btn delete-action">
                    <div class="row">
                        <div class="col-6">
                            <form method="POST" id="url-delete">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-primary continue-btn w-100" type="submit">Hapus</button>
                            </form>
                        </div>
                        <div class="col-6">
                            <button type="button" data-dismiss="modal" class="btn btn-primary">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Delete Surat Masuk Modal end --}}

{{-- modal end --}}
@endsection
@push ('page-scripts')
<script>
    $(document).ready(function() {
        var table = $("#table-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('surat_masuk.json') }}",
            columns: [
                {
                    data: "id",
                    name: "id",
                },
                {
                    data: "tanggal",
                    name: "tanggal",
                },
                {
                    data: "no_surat",
                    name: "no_surat"
                },
                {
                    data: "jenis_surat",
                    name: "jenis_surat.nama_surat"
                },
                {
                    data: "image",
                    name: "image"
                },
                {
                    data: "perihal",
                    name: "perihal"
                },
                {
                    data: "action",
                    name: "action",
                    className: 'text-center'
                },
            ],
            order: [[ 0, "desc" ]]
        });
        table.on('draw.dt', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied', order: 'applied', page: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });

        // add surat masuk
        $('.add-btn').on('click', function(){            
            $('#modal_surat_masuk .modal-title span').html('Tambah');
            $('#modal_surat_masuk #id').val('');
            $('#modal_surat_masuk form').trigger('reset');


            var myModal = new bootstrap.Modal(document.getElementById('modal_surat_masuk'))
            myModal.show()
        });

        // edit surat masuk
        $("#table-datatable").on("click", ".btn-edit", function(e) {
            e.preventDefault();
            data = $(this).data();

            $.each(data, function(index, value){
                $('#modal_surat_masuk #'+index).val(value);
            })

            $('#modal_surat_masuk'+' .modal-title span').html('Edit');

            var myModal = new bootstrap.Modal(document.getElementById('modal_surat_masuk'))
            myModal.show()
        });

        // detail surat masuk
        $("#table-datatable").on("click", ".btn-detail", function(e) {
            e.preventDefault();
            data = $(this).data();
    
            $.each(data, function(index, value){
                $('#modal_surat_masuk_detail #'+index).html(value);
            })

            $('#modal_surat_masuk_detail #image').attr('src', data.image);
            
            var myModal = new bootstrap.Modal(document.getElementById('modal_surat_masuk_detail'))
            myModal.show()
        });

        // delete surat masuk
        $("#table-datatable").on("click", ".btn-delete", function(e) {
            e.preventDefault();
            const id = $(this).data("id");
            $('#delete_surat_masuk #url-delete').attr('action', "{{ url('surat_masuk') }}/"+id);

    swal({
        title: 'Kamu Yakin Mau Hapus?',
        text: 'Jika di Hapus, Data akan hilang!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        swal('Data Berhasil diHapus :)', {
          icon: 'success',
        });
        $(`#url-delete`).submit();
        } else {
        swal('Data Kamu Aman!');
        }
      });
            // var myModal = new bootstrap.Modal(document.getElementById('delete_surat_masuk'))
            // myModal.show()
        });

    });
</script>
@endpush