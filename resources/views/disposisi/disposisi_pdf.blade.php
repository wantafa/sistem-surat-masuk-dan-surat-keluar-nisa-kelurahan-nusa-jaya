<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DISPOSISI </title>
</head>
<style>
    .center {
        text-align: center;
    }

    .tbl-center {
        margin-left: 40px;
        margin-right: auto;
    }

</style>
<body>

    {{-- <img src="{{ public_path('assets/img/kop.jpg') }}" height="15%" width="100%"> --}}
    <div style="text-align: center;font-size:35px;margin-top:20px;">
        <span><strong>KARTU PENERUS DISPOSISI</strong></span><br>
    </div>


    <table class="tbl-center" style="border-top: 2px solid; border-bottom: 2px solid">
        <tr>
            <td>
                <div style="text-align: center;">
                    <span>INDEK :</span>
                    <span>........................</span><br>
                    <span style="margin-left: 62px">........................</span><br>
                    <span style="margin-left: 62px">........................</span>
                </div>
            </td>
            <td style="padding-right: 250px;">   </td>
            <td>
                <div style="text-align: center">
                    <span>TANGGAL PENYELESAIAN</span><br>
                    <span style="">{{ \Carbon\Carbon::create($disposisi->tgl_penyelesaian)->format('d/m/Y') }}</span><br>
                    <span style="">...........................................</span>
                </div>
            </td>
        </tr>
    </table>

    <table class="tbl-center">
        <tr>
            <td>
                <div>
                    <span>INDEK :</span>
                    <span>........................</span><br>
                    <span style="margin-left: 62px">........................</span><br>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <span>Tgl/No  :</span>
                    <span>{{ \Carbon\Carbon::create($disposisi->tanggal)->format('d/m/Y') }}, {{ $disposisi->no_surat }}</span><br>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="">
                    <span style="margin-right: 20px">Asal</span>
                    <span>: {{ $disposisi->asal }}</span><br>
                </div>
            </td>
        </tr>

    </table>

    <table class="tbl-center" style="border-top: 2px solid; border-bottom: 2px solid">
        <tr>
            <td style="padding-right: 135px; vertical-align: top">
                    <span>INTRUKSI / INFORMASI</span><br><br>
                    <span>{{ $disposisi->intruksi }}</span>
            </td>
            <td style="border-right: 2px solid">   </td>
            <td>
                <div style="padding-right: 105px; padding-left: 20px ">
                    <span>DITERUSKAN KEPADA</span><br><br>
                    <span>1. {{ $disposisi->diteruskan }}</span><br><br>
                    <span>2.</span><br><br>
                    <span>3.</span><br><br>
                    <span>4.</span><br><br>
                    <span>5.</span><br><br>
                    <span>6.</span><br><br>
                </div>
            </td>
        </tr>
    </table>

    <table class="tbl-center">
        <tr>
            <td style="vertical-align: top">
                    <p style="margin-bottom: 150px; margin-top: -1px">Sesudah digunakan harap segera dikembalikan</p><br><br>
                    <span>Kepada :</span><br>
                    <span>Tanggal :</span><br><br>
            </td>
            <td></td>
            <td style="vertical-align: top">
                <div>
                    <span>1. Kepada bawahan "Intruksi" <br> dan atau "Informasi"</span><br><br>
                    <span>2. Kepada atasan "Informasi" coret "Intruksi"</span><br><br>
                </div>
            </td>
        </tr>
    </table>

</body>
</html>