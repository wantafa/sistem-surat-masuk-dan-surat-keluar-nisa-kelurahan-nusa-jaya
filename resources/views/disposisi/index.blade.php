@extends ('layouts.base')
@section('title', 'Disposisi')
@section('content')

  {{-- content start --}}
  <div class="page-header">
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Disposisi</a>
        </li>
    </ul>
</div>
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h4 class="card-title">Disposisi</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-datatable" class="table table-striped custom-table w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Surat</th>
                                <th>Asal</th>
                                <th>Intruksi</th>
                                <th>Diteruskan</th>
                                <th>Tanggal Penyelesaian</th>
                                <th class="text-center" width="140">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- content end --}}

{{-- modal start --}}


{{-- modal end --}}
@endsection
@push ('page-scripts')
<script>
    $(document).ready(function() {
        var table = $("#table-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('disposisi.json') }}",
            columns: [
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                },
                {
                    data: "no_surat",
                    name: "no_surat"
                },
                {
                    data: "asal",
                    name: "asal"
                },
                {
                    data: "intruksi",
                    name: "intruksi"
                },
                {
                    data: "diteruskan",
                    name: "diteruskan"
                },
                {
                    data: "tgl_penyelesaian",
                    name: "tgl_penyelesaian"
                },
                {
                    data: "action",
                    name: "action",
                    className: 'text-center'
                },
            ],
            order: [[ 0, "desc" ]]
        });
        table.on('draw.dt', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied', order: 'applied', page: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });



        // Acc Disposisi
        $("#table-datatable").on("click", ".btn-acc", function(e) {
            e.preventDefault();
            const id = $(this).data("id");

            swal({
            title: 'Kamu yakin?',
            text: "ingin Approve Disposisi ini?",
            icon: 'warning',
            buttons: true,
            }).then((result) => {
            if (result) {
                swal(
                'Berhasil!',
                'Disposisi berhasil diApprove',
                'success'
                )
            location.assign("/disposisi/acc/"+id);
            }
            });
        });
    });
</script>
@endpush