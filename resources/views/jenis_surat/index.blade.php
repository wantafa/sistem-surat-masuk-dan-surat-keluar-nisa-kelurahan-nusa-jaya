@extends ('layouts.base')
@section('title', 'Jenis Surat')
@section('content')

  {{-- content start --}}
  <div class="page-header">
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Jenis Surat</a>
        </li>
    </ul>
</div>
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h4 class="card-title">Jenis Surat</h4>
                    <button class="btn btn-primary btn-round btn-sm ml-auto add-btn" data-toggle="modal" data-target="#modal_jenis_surat">
                        <i class="fa fa-plus"></i>
                        Tambah
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-datatable" class="table table-striped custom-table w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Surat</th>
                                <th>Jenis Surat</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- content end --}}

{{-- modal start --}}

{{-- Jenis Surat Modal start --}}

<div id="modal_jenis_surat" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>Tambah</span> Jenis Surat</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('jenis_surat.store') }}">
                    @csrf
                    <input type="hidden" id="id" name="id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Nama Surat<span class="text-danger">*</span></label>
                                <input class="form-control" id="nama_surat" name="nama_surat" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Jenis Surat<span class="text-danger">*</span></label>
                                <select name="jenis_surat" class="form-control" id="jenis_surat">
                                    <option value="">- Pilih -</option>
                                    <option value="Surat Masuk">Surat Masuk</option>
                                    <option value="Surat Keluar">Surat Keluar</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer no-bd">
                        <button class="btn btn-primary submit-btn">Simpan</button>
                        <button class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Jenis Surat Modal end --}}

{{--  Jenis Surat Detail Modal start --}}
<div id="modal_jenis_surat_detail" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>Detail</span> Jenis Surat</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped table-border">
                            <tbody>
                                <tr>
                                    <td>NIK :</td>
                                    <td class="text-right" id="nik"></td>
                                </tr>
                                <tr>
                                    <td>Nama :</td>
                                    <td class="text-right" id="nama"></td>
                                </tr>
                                <tr>
                                    <td>Tempat Lahir :</td>
                                    <td class="text-right" id="tempat_lahir"></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Lahir :</td>
                                    <td class="text-right" id="tgl_lahir"></td>
                                </tr>
                                <tr>
                                    <td>Jenis Kelamin :</td>
                                    <td class="text-right" id="jenis_kelamin"></td>
                                </tr>
                                <tr>
                                    <td>Alamat :</td>
                                    <td class="text-right" id="alamat"></td>
                                </tr>
                                <tr>
                                    <td>Agama :</td>
                                    <td class="text-right" id="agama"></td>
                                </tr>
                                <tr>
                                    <td>Status Perkawinan :</td>
                                    <td class="text-right" id="status_perkawinan"></td>
                                </tr>
                                <tr>
                                    <td>Kewarganegaraan :</td>
                                    <td class="text-right" id="kewarganegaraan"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--  Jenis Surat Detail Modal end --}}


{{-- Delete Jenis Surat Modal start --}}

<div class="modal custom-modal fade" id="delete_jenis_surat" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLongTitle">Hapus</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <h4 class="text-center">Anda yakin ingin hapus?</h4>
                <div class="modal-btn delete-action">
                    <div class="row">
                        <div class="col-6">
                            <form method="POST" id="url-delete">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-primary continue-btn w-100" type="submit">Hapus</button>
                            </form>
                        </div>
                        <div class="col-6">
                            <button type="button" data-dismiss="modal" class="btn btn-primary">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Delete Jenis Surat Modal end --}}

{{-- modal end --}}
@endsection

@push ('page-scripts')
<script>
    $(document).ready(function() {
        var table = $("#table-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('jenis_surat.json') }}",
            columns: [
                {
                    data: "id",
                    name: "id",
                },
                {
                    data: "nama_surat",
                    name: "nama_surat",
                },
                {
                    data: "jenis_surat",
                    name: "jenis_surat"
                },
                {
                    data: "action",
                    name: "action",
                    className: 'text-center'
                },
            ],
            order: [[ 0, "desc" ]]
        });
        table.on('draw.dt', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied', order: 'applied', page: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });

        // add jenis surat
        $('.add-btn').on('click', function(){            
            $('#modal_jenis_surat .modal-title span').html('Tambah');
            $('#modal_jenis_surat #id').val('');
            $('#modal_jenis_surat form').trigger('reset');


            var myModal = new bootstrap.Modal(document.getElementById('modal_jenis_surat'))
            myModal.show()
        });

        // edit jenis surat
        $("#table-datatable").on("click", ".btn-edit", function(e) {
            e.preventDefault();
            data = $(this).data();

            $.each(data, function(index, value){
                $('#modal_jenis_surat #'+index).val(value);
            })

            $('#modal_jenis_surat'+' .modal-title span').html('Edit');

            var myModal = new bootstrap.Modal(document.getElementById('modal_jenis_surat'))
            myModal.show()
        });

        // detail jenis surat
        $("#table-datatable").on("click", ".btn-detail", function(e) {
            e.preventDefault();
            data = $(this).data();
    
            $.each(data, function(index, value){
                $('#modal_jenis_surat_detail #'+index).html(value);
            })
    
            var myModal = new bootstrap.Modal(document.getElementById('modal_jenis_surat_detail'))
            myModal.show()
        });


        // delete jenis surat
        $("#table-datatable").on("click", ".btn-delete", function(e) {
            e.preventDefault();
            const id = $(this).data("id");
            $('#delete_jenis_surat #url-delete').attr('action', "{{ url('jenis_surat') }}/"+id);

    swal({
        title: 'Kamu Yakin Mau Hapus?',
        text: 'Jika di Hapus, Data akan hilang!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        swal('Data Berhasil diHapus :)', {
          icon: 'success',
        });
        $(`#url-delete`).submit();
        } else {
        swal('Data Kamu Aman!');
        }
      });
            // var myModal = new bootstrap.Modal(document.getElementById('delete_jenis_surat'))
            // myModal.show()
        });

    });
</script>
@endpush