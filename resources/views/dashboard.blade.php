@extends ('layouts.base')
@section('title', 'Dashboard')
@section('content')


<div class="page-header">
    <h4 class="page-title">Dashboard</h4>
    {{-- <div class="btn-group btn-group-page-header ml-auto">
        <button type="button" class="btn btn-light btn-round btn-page-header-dropdown dropdown-toggle"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-ellipsis-h"></i>
        </button>
    </div> --}}
</div>
<div class="alert alert-warning" role="alert">
    <i class="fas fa-bell"></i> <b>Halo {{ Auth::user()->name }}! Selamat Datang :D </b>
</div>
<div id="MyClockDisplay" class="clock" onload="showTime()" style="float: right"></div>
<div class="row">
    <div class="col-md-6">
        <h3>Total Surat Masuk</h3>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-round">
            <div class="card-body ">
                <div class="row align-items-center">
                    <div class="col-icon">
                        <div class="icon-big text-center icon-primary bubble-shadow-small">
                            <i class="far fa-file-alt"></i>
                        </div>
                    </div>
                    <div class="col col-stats ml-3 ml-sm-0">
                        <div class="numbers">
                            <p class="card-category">Lurah</p>
                            <h4 class="card-title">{{ $lurah }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-round">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-icon">
                        <div class="icon-big text-center icon-info bubble-shadow-small">
                            <i class="far fa-file-alt"></i>
                        </div>
                    </div>
                    <div class="col col-stats ml-3 ml-sm-0">
                        <div class="numbers">
                            <p class="card-category">Sekretariat</p>
                            <h4 class="card-title">{{ $sekretaris }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-round">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-icon">
                        <div class="icon-big text-center icon-success bubble-shadow-small">
                            <i class="far fa-file-alt"></i>
                        </div>
                    </div>
                    <div class="col col-stats ml-3 ml-sm-0">
                        <div class="numbers">
                            <p class="card-category">Tata Pemerintahan</p>
                            <h4 class="card-title">{{ $tata_pemerintahan }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-round">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-icon">
                        <div class="icon-big text-center icon-secondary bubble-shadow-small">
                            <i class="far fa-file-alt"></i>
                        </div>
                    </div>
                    <div class="col col-stats ml-3 ml-sm-0">
                        <div class="numbers">
                            <p class="card-category">Ekonomi dan Pembangunan</p>
                            <h4 class="card-title">{{ $ekonomi }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-round">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-icon">
                        <div class="icon-big text-center icon-warning bubble-shadow-small">
                            <i class="far fa-file-alt"></i>
                        </div>
                    </div>
                    <div class="col col-stats ml-3 ml-sm-0">
                        <div class="numbers">
                            <p class="card-category">Pemberdayaan Masyarakat</p>
                            <h4 class="card-title">{{ $pemberdayaan }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Surat Keluar --}}
<div class="row">
    <div class="col-md-6">
        <h3>Total Surat Keluar</h3>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-round">
            <div class="card-body ">
                <div class="row align-items-center">
                    <div class="col-icon">
                        <div class="icon-big text-center icon-primary bubble-shadow-small">
                            <i class="far fa-file-alt"></i>
                        </div>
                    </div>
                    <div class="col col-stats ml-3 ml-sm-0">
                        <div class="numbers">
                            <p class="card-category">SKCK</p>
                            <h4 class="card-title">{{ $skck }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-round">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-icon">
                        <div class="icon-big text-center icon-info bubble-shadow-small">
                            <i class="far fa-file-alt"></i>
                        </div>
                    </div>
                    <div class="col col-stats ml-3 ml-sm-0">
                        <div class="numbers">
                            <p class="card-category">Pindah Rumah</p>
                            <h4 class="card-title">{{ $pindah }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-round">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-icon">
                        <div class="icon-big text-center icon-success bubble-shadow-small">
                            <i class="far fa-file-alt"></i>
                        </div>
                    </div>
                    <div class="col col-stats ml-3 ml-sm-0">
                        <div class="numbers">
                            <p class="card-category">Belum Memiliki Rumah</p>
                            <h4 class="card-title">{{ $blm_pny_rmh }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-3">
        <div class="card card-stats card-round">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-icon">
                        <div class="icon-big text-center icon-secondary bubble-shadow-small">
                            <i class="far fa-file-alt"></i>
                        </div>
                    </div>
                    <div class="col col-stats ml-3 ml-sm-0">
                        <div class="numbers">
                            <p class="card-category">Surat Nikah</p>
                            <h4 class="card-title">{{ $nikah }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card card-stats card-round">
            <div class="card-body">
                <p><h2>Visi</h2></p>
                    <p><h2 class="text-center">Terdepan dalam pelayanan prima menuju kelurahan industri, perdagangan, jasa dan pemukiman yang berakhlakul karimah</h2></p>
                    
                    <p><h2>Misi</h2></p>
                    <p><h2>1. Meningkatkan kapasitas kelembagaan dan kualitas sumber daya aparatur <br>
                        2. meningkatkan kapasitas pelayanan <br>
                        3. meningkatkan kapasitas pelayanan administrasi kependudukan dan catatan sipil <br>
                        4. meningkatkan ketentraman dan ketertiban masyarakat</h2></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-head-row">
                    <div class="card-title">Total Surat Masuk</div>
                    <div class="card-tools">
                        {{-- <a href="#" class="btn btn-info btn-border btn-round btn-sm mr-2">
                            <span class="btn-label">
                                <i class="fa fa-pencil"></i>
                            </span> Export
                        </a> --}}
                        {{-- <a href="#" class="btn btn-info btn-border btn-round btn-sm">
                            <span class="btn-label">
                                <i class="fa fa-print"></i>
                            </span> Print
                        </a> --}}
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="chart-container" style="min-height: 375px">
                    <canvas id="myChart"></canvas>
                </div>
                <div id="myChart"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-head-row">
                    <div class="card-title">Total Surat Keluar</div>
                    <div class="card-tools">
                        {{-- <a href="#" class="btn btn-info btn-border btn-round btn-sm mr-2">
                            <span class="btn-label">
                                <i class="fa fa-pencil"></i>
                            </span> Export
                        </a> --}}
                        {{-- <a href="#" class="btn btn-info btn-border btn-round btn-sm">
                            <span class="btn-label">
                                <i class="fa fa-print"></i>
                            </span> Print
                        </a> --}}
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="chart-container" style="min-height: 375px">
                    <canvas id="keluar"></canvas>
                </div>
                <div id="keluar"></div>
            </div>
        </div>
    </div>
    {{-- <div class="col-md-4">
        <div class="card card-secondary">
            <div class="card-header">
                <div class="card-title">Daily Sales</div>
                <div class="card-category">March 25 - April 02</div>
            </div>
            <div class="card-body pb-0">
                <div class="mb-4 mt-2">
                    <h1>$4,578.58</h1>
                </div>
                <div class="pull-in">
                    <canvas id="dailySalesChart"></canvas>
                </div>
            </div>
        </div>
        <div class="card card-info bg-info-gradient">
            <div class="card-body">
                <h4 class="mb-1 fw-bold">Tasks Progress</h4>
                <div id="task-complete" class="chart-circle mt-4 mb-3"></div>
            </div>
        </div>
    </div> --}}
</div>

<script src="https://demo.getstisla.com/assets/modules/chart.min.js"></script>
<script>
"use strict";

var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    
    datasets: [{
      label: 'Jumlah',
      data: [
        @foreach ($surat_masuk as $item)
      "{{ $item->total }}",
  @endforeach
      ],
      borderWidth: 2,
      backgroundColor: [
            '#A77D54',
            '#84735E',
            '#885A3A',
            '#C9B09A',
            '#DBD0BD',
            ],
      borderColor: '#DBD0BD',
    }],
    labels: [
    @foreach ($surat_masuk as $item)
      "{{ $item->nama_surat }}",
    @endforeach
            ],
  },
  options: {
    responsive: true,
    legend: {
      position: 'botom',
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
        }
      }],
    },
  }
});

</script>

<script>
    "use strict";

    var ctx = document.getElementById("keluar").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        
        datasets: [{    
          label: 'Jumlah',
          data: [
            @foreach ($surat_keluar as $item)
          "{{ $item->total }}",
      @endforeach
          ],
          borderWidth: 2,
          backgroundColor: [
                    '#A77D54',
                    '#84735E',
                    '#885A3A',
                    '#C9B09A',
                    '#DBD0BD',
                ],
          borderColor: '#DBD0BD',
        }],
        labels: [
        @foreach ($surat_keluar as $item)
          "{{ strtolower($item->nama_surat) }}",
        @endforeach
                ],
      },
      options: {
        responsive: true,
        legend: {
          position: 'botom',
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
            }
          }],
        },
      }
    });
    
    </script>

@endsection
@push ('page-scripts')
{{-- <script>
	$('#myModal').modal(options)
</script> --}}
@endpush
