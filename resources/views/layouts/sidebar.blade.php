<div class="sidebar">

    {{-- <div class="sidebar-background"></div> --}}
    <div class="sidebar-wrapper scrollbar-inner">
        <div class="sidebar-content">
            {{-- <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="../assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            Hizrian
                            <span class="user-level">Administrator</span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="#profile">
                                    <span class="link-collapse">My Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="#edit">
                                    <span class="link-collapse">Edit Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="#settings">
                                    <span class="link-collapse">Settings</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div> --}}
            <ul class="nav">
                <li class="nav-item {{ Request::is('dashboard') ? 'active' : '' }}">
                    <a href="/">
                        <i class="flaticon-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                @if (Auth::user()->role == 'staff')
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Menu</h4>
                </li>

                <li class="nav-item {{ Request::is('manajemen_user', 'penduduk','jenis_surat') ? 'active submenu' : '' }}">
                    <a data-toggle="collapse" href="#base">
                        <i class="flaticon-layers"></i>
                        <p>Data Master</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ Request::is('manajemen_user','penduduk','jenis_surat') ? 'show' : '' }}" id="base">
                        <ul class="nav nav-collapse">
                            <li class=" {{ Request::is('manajemen_user') ? 'active' : '' }}">
                                <a href="{{ route('manajemen_user.index') }}">
                                    <span class="sub-item">Data User</span>
                                </a>
                            </li>
                            <li class="{{ Request::is('penduduk') ? 'active' : '' }}">
                                <a href="{{ route('penduduk.index') }}">
                                    <span class="sub-item">Data Penduduk</span>
                                </a>
                            </li>
                            {{-- <li class="{{ Request::is('jenis_surat') ? 'active' : '' }}">
                                <a href="{{ route('jenis_surat.index') }}">
                                    <span class="sub-item">Jenis Surat</span>
                                </a>
                            </li> --}}
                        </ul>
                    </div>
                </li>
                @endif
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Surat</h4>
                </li>
                @if (Auth::user()->role == 'staff')
                <li class="nav-item {{ Request::is('surat_masuk', 'surat_keluar') ? 'active submenu' : '' }}">
                    <a data-toggle="collapse" href="#surat">
                        <i class="flaticon-layers"></i>
                        <p>Data Surat</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ Request::is('surat_masuk', 'surat_keluar') ? 'show' : '' }}" id="surat">
                        <ul class="nav nav-collapse">
                            <li class=" {{ Request::is('surat_masuk') ? 'active' : '' }}">
                                <a href="{{ route('surat_masuk.index') }}">
                                    <span class="sub-item">Surat Masuk</span>
                                </a>
                            </li>
                            <li class="{{ Request::is('surat_keluar') ? 'active' : '' }}">
                                <a href="{{ route('surat_keluar.index') }}">
                                    <span class="sub-item">Surat Keluar</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                @endif
                <li class="nav-item {{ Request::is('disposisi') ? 'active' : '' }}">
                    <a href="{{ route('disposisi') }}">
                        <i class="fas fa-file-archive"></i>
                        <p>Disposisi</p>
                    </a>
                </li>
                @if (Auth::user()->role == 'staff')
                <li class="nav-item {{ Request::is('cetak_surat') ? 'active' : '' }}">
                    <a href="{{ route('cetak_surat') }}">
                        <i class="flaticon-calendar"></i>
                        <p>Cetak Surat</p>
                    </a>
                </li>
                @endif
                <li class="nav-item {{ Request::is('laporan_surat_masuk', 'report/surat_keluar', 'report/disposisi') ? 'active submenu' : '' }}">
                    <a data-toggle="collapse" href="#laporan">
                        <i class="flaticon-layers"></i>
                        <p>Laporan</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ Request::is('report/surat_masuk', 'report/surat_keluar', 'report/disposisi') ? 'show' : '' }}" id="laporan">
                        <ul class="nav nav-collapse">
                            <li class=" {{ Request::is('report/surat_masuk') ? 'active' : '' }}">
                                <a href="{{ route('report.surat_masuk') }}">
                                    <span class="sub-item">Laporan Surat Masuk</span>
                                </a>
                            </li>
                            <li class="{{ Request::is('report/surat_keluar') ? 'active' : '' }}">
                                <a href="{{ route('report.surat_keluar') }}">
                                    <span class="sub-item">Laporan Surat Keluar</span>
                                </a>
                            </li>
                            <li class="{{ Request::is('report/disposisi') ? 'active' : '' }}">
                                <a href="{{ route('report.disposisi') }}">
                                    <span class="sub-item">Laporan Disposisi</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>