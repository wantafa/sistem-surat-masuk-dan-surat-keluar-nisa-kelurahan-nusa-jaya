@extends ('layouts.base')
@section('title', 'Surat Keluar')
@section('content')

  {{-- content start --}}
  <div class="page-header">
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Surat Keluar</a>
        </li>
    </ul>
</div>
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h4 class="card-title">Surat Keluar</h4>
                    <button class="btn btn-primary btn-round ml-auto add-btn" data-toggle="modal" data-target="#modal_surat_keluar">
                        <i class="fa fa-plus"></i>
                        Tambah
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-datatable" class="table table-striped custom-table w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Nomor Surat</th>
                                <th>Jenis Surat</th>
                                <th>Lampiran</th>
                                <th>Perihal</th>
                                <th width="100">Status</th>
                                <th width="120" class="text-center">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- content end --}}

{{-- modal start --}}

{{-- Surat Keluar Modal start --}}

<div id="modal_surat_keluar" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>Tambah</span> Surat Keluar</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('surat_keluar.store') }}">
                    @csrf
                    <input type="hidden" id="id" name="id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Tanggal<span class="text-danger">*</span></label>
                                <input class="form-control" id="tanggal" name="tanggal" type="date" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Nomor Surat<span class="text-danger">*</span></label>
                                <input class="form-control" id="no_surat" name="no_surat" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Jenis Surat<span class="text-danger">*</span></label>
                                <select name="jenis_surat_id" id="jenis_surat_id" class="form-control">
                                    <option>- Pilih -</option>
                                    @foreach ($jenis_surat as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_surat }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Lampiran<span class="text-danger">*</span></label>
                                <input class="form-control" id="lampiran" name="lampiran" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Perihal<span class="text-danger">*</span></label>
                                <input class="form-control" id="perihal" name="perihal" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-form-label">Verification<span class="text-danger">*</span></label>
                                <input class="form-control" id="verification" name="verification" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-form-label">Nama Lurah<span class="text-danger">*</span></label>
                                <select name="nama_lurah" id="nama_lurah" class="form-control">
                                    <option>- Pilih -</option>
                                    <option value="an/Lurah Nusa Jaya">a.n LURAH NUSA JAYA</option>
                                    <option value="Lurah Nusa Jaya">LURAH NUSA JAYA</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="jenis" id="jenis">
                        {{-- FORM KEBUTUHAN SURAT KELUAR --}}
                        <div class="col-md-6">
                            <div class="form-group penduduk">
                                <label class="col-form-label">Penduduk<span class="text-danger">*</span></label>
                                <select name="penduduk_id" id="penduduk_id" class="form-control">
                                    <option>- Pilih -</option>
                                    @foreach ($penduduk as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group keterangan_skck">
                                <label class="col-form-label">Keterangan<span class="text-danger">*</span></label>
                                <input class="form-control " id="keterangan_skck" name="keterangan_skck" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group keterangan_pindah">
                                <label class="col-form-label">Keterangan<span class="text-danger">*</span></label>
                                <input class="form-control " id="keterangan_pindah" name="keterangan_pindah" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group no_kk">
                                <label class="col-form-label">Nomor Kartu Keluarga<span class="text-danger">*</span></label>
                                <input class="form-control" id="no_kartu_keluarga" name="no_kartu_keluarga" type="number" min="1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group alamat_pindah">
                                <label class="col-form-label">Alamat Pindah<span class="text-danger">*</span></label>
                                <input class="form-control" id="alamat_pindah" name="alamat_pindah" type="text">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group jml_pengikut">
                                <label class="col-form-label">Jumlah Pengikut<span class="text-danger">*</span></label>
                                <input class="form-control" id="jumlah_pengikut" name="jumlah_pengikut" type="number" min="1">
                            </div>
                        </div>
                        <div class="col-md-12 nikah">
                            <ul class="nav nav-pills nav-secondary nav-pills-no-bd d-flex justify-content-center" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab-nobd" data-toggle="pill" href="#pills-home-nobd" role="tab" aria-controls="pills-home-nobd" aria-selected="true">N1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab-nobd" data-toggle="pill" href="#pills-profile-nobd" role="tab" aria-controls="pills-profile-nobd" aria-selected="false">N4</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab-nobd" data-toggle="pill" href="#pills-contact-nobd" role="tab" aria-controls="pills-contact-nobd" aria-selected="false">N5</a>
                                </li>
                            </ul>
                            <div class="tab-content " id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home-nobd" role="tabpanel" aria-labelledby="pills-home-tab-nobd">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-form-label">Calon<span class="text-danger">*</span></label>
                                                <select name="penduduk_id_calon" id="penduduk_id_calon" class="form-control">
                                                    <option>- Pilih -</option>
                                                    @foreach ($penduduk as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-form-label">Ayah<span class="text-danger">*</span></label>
                                                <select name="penduduk_id_ayah" id="penduduk_id_ayah" class="form-control">
                                                    <option>- Pilih -</option>
                                                    @foreach ($penduduk as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-form-label">Ibu<span class="text-danger">*</span></label>
                                                <select name="penduduk_id_ibu" id="penduduk_id_ibu" class="form-control">
                                                    <option>- Pilih -</option>
                                                    @foreach ($penduduk as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-profile-nobd" role="tabpanel" aria-labelledby="pills-profile-tab-nobd">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Calon Suami<span class="text-danger">*</span></label>
                                                <select name="penduduk_id_calon_suami" id="penduduk_id_calon_suami" class="form-control">
                                                    <option>- Pilih -</option>
                                                    @foreach ($penduduk as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="col-form-label">Calon Istri<span class="text-danger">*</span></label>
                                                <select name="penduduk_id_calon_istri" id="penduduk_id_calon_istri" class="form-control">
                                                    <option>- Pilih -</option>
                                                    @foreach ($penduduk as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-contact-nobd" role="tabpanel" aria-labelledby="pills-contact-tab-nobd">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-form-label">Calon Suami<span class="text-danger">*</span></label>
                                                <select name="penduduk_id_calon_suami" id="penduduk_id_calon_suami" class="form-control">
                                                    <option>- Pilih -</option>
                                                    @foreach ($penduduk as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-form-label">Calon Istri<span class="text-danger">*</span></label>
                                                <select name="penduduk_id_calon_istri" id="penduduk_id_calon_istri" class="form-control">
                                                    <option>- Pilih -</option>
                                                    @foreach ($penduduk as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-form-label">Ayah Calon<span class="text-danger">*</span></label>
                                                <select name="penduduk_id_ayah_calon" id="penduduk_id_ayah_calon" class="form-control">
                                                    <option>- Pilih -</option>
                                                    @foreach ($penduduk as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-form-label">Orang Tua Ayah<span class="text-danger">*</span></label>
                                                <input class="form-control" id="ortu_ayah" name="ortu_ayah" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-form-label">Ibu Calon<span class="text-danger">*</span></label>
                                                <select name="penduduk_id_ibu_calon" id="penduduk_id_ibu_calon" class="form-control">
                                                    <option>- Pilih -</option>
                                                    @foreach ($penduduk as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-form-label">Orang Tua Ibu<span class="text-danger">*</span></label>
                                                <input class="form-control" id="ortu_ibu" name="ortu_ibu" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-form-label">Orang Tua Calon<span class="text-danger">*</span></label>
                                                <input class="form-control" id="ortu_calon" name="ortu_calon" type="text">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer no-bd">
                        <button class="btn btn-primary submit-btn">Simpan</button>
                        <button class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Surat Keluar Modal end --}}

{{--  Surat Keluar Detail Modal start --}}
<div id="modal_surat_keluar_detail" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>Detail</span> Surat Keluar</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped table-border">
                            <tbody>
                                <tr>
                                    <td>Tanggal :</td>
                                    <td class="text-right" id="tanggal"></td>
                                </tr>
                                <tr>
                                    <td>Nomor Surat :</td>
                                    <td class="text-right" id="no_surat"></td>
                                </tr>
                                <tr>
                                    <td>Jenis Surat :</td>
                                    <td class="text-right" id="jenis_surat"></td>
                                </tr>
                                <tr>
                                    <td>Perihal :</td>
                                    <td class="text-right" id="perihal"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--  Surat Keluar Detail Modal end --}}

{{-- Delete Surat Keluar Modal start --}}

<div class="modal custom-modal fade" id="delete_surat_keluar" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLongTitle">Hapus</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <h4 class="text-center">Anda yakin ingin hapus?</h4>
                <div class="modal-btn delete-action">
                    <div class="row">
                        <div class="col-6">
                            <form method="POST" id="url-delete">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-primary continue-btn w-100" type="submit">Hapus</button>
                            </form>
                        </div>
                        <div class="col-6">
                            <button type="button" data-dismiss="modal" class="btn btn-primary">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Delete Surat Keluar Modal end --}}

{{-- modal end --}}
@endsection
@push ('page-scripts')
<script>
    $(document).ready(function() {
        var table = $("#table-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('surat_keluar.json') }}",
            columns: [
                {
                    data: "id",
                    name: "id",
                },
                {
                    data: "tanggal",
                    name: "tanggal",
                },
                {
                    data: "no_surat",
                    name: "no_surat"
                },
                {
                    data: "jenis_surat",
                    name: "jenis_surat.nama_surat"
                },
                {
                    data: "lampiran",
                    name: "lampiran"
                },
                {
                    data: "perihal",
                    name: "perihal"
                },
                {
                    data: "status",
                    name: "status",
                    className: 'text-center'
                },
                {
                    data: "action",
                    name: "action",
                    className: 'text-center'
                },
            ],
            order: [[ 0, "desc" ]]
        });
        table.on('draw.dt', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied', order: 'applied', page: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });

        // add surat keluar
        $('.add-btn').on('click', function(){            
            $('#modal_surat_keluar .modal-title span').html('Tambah');
            $('#modal_surat_keluar #id').val('');
            $('#modal_surat_keluar form').trigger('reset');


            var myModal = new bootstrap.Modal(document.getElementById('modal_surat_keluar'))
            myModal.show()
        });

        // edit surat keluar
        $("#table-datatable").on("click", ".btn-edit", function(e) {
            e.preventDefault();
            data = $(this).data();

            if (data.jenis_surat == "SURAT PENGANTAR CATATAN KEPOLISIAN") {
                $('.penduduk, .keterangan_skck').show();
                $('.nikah, .keterangan_pindah, .no_kk, .alamat_pindah, .jml_pengikut').hide();
                $('#jenis').val('skck');
            }

            if (data.jenis_surat == "SURAT PENGANTAR PINDAH ANTAR KOTA/KABUPATEN/PROVINSI") {
                $('.penduduk, .keterangan_pindah, .no_kk, .alamat_pindah, .jml_pengikut').show();
                $('.nikah, .keterangan_skck').hide();
                $('#jenis').val('pindah');
            } 
            
            if (data.jenis_surat == "SURAT KETERANGAN BELUM MEMILIKI RUMAH") {
                $('.penduduk').show();
                $('.keterangan_skck, .keterangan_pindah, .no_kk, .alamat_pindah, .jml_pengikut, .nikah').hide();
                $('#jenis').val('blm_pny_rmh');
            }
            
            if (data.jenis_surat == "SURAT NIKAH") {
                $('.penduduk, .nikah').show();
                $('.keterangan_skck, .keterangan_pindah, .no_kk, .alamat_pindah, .jml_pengikut').hide();
                $('#jenis').val('nikah');
            }

            $.each(data, function(index, value){
                $('#modal_surat_keluar #'+index).val(value);
            })

            $('#modal_surat_keluar'+' .modal-title span').html('Edit');

            var myModal = new bootstrap.Modal(document.getElementById('modal_surat_keluar'))
            myModal.show()
        });

        // detail surat keluar
        $("#table-datatable").on("click", ".btn-detail", function(e) {
            e.preventDefault();
            data = $(this).data();
    
            $.each(data, function(index, value){
                $('#modal_surat_keluar_detail #'+index).html(value);
            })
    
            var myModal = new bootstrap.Modal(document.getElementById('modal_surat_keluar_detail'))
            myModal.show()
        });

        // delete surat keluar
        $("#table-datatable").on("click", ".btn-delete", function(e) {
            e.preventDefault();
            const id = $(this).data("id");
            $('#delete_surat_keluar #url-delete').attr('action', "{{ url('surat_keluar') }}/"+id);

    swal({
        title: 'Kamu Yakin Mau Hapus?',
        text: 'Jika di Hapus, Data akan hilang!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        swal('Data Berhasil diHapus :)', {
          icon: 'success',
        });
        $(`#url-delete`).submit();
        } else {
        swal('Data Kamu Aman!');
        }
      });
            // var myModal = new bootstrap.Modal(document.getElementById('delete_surat_keluar'))
            // myModal.show()
        });

        // Acc Surat Keluar
        $("#table-datatable").on("click", ".btn-acc", function(e) {
            e.preventDefault();
            const id = $(this).data("id");

            swal({
            title: 'Kamu yakin?',
            text: "ingin Approve Surat Keluar ini?",
            icon: 'warning',
            buttons: true,
            }).then((result) => {
            if (result) {
                swal(
                'Berhasil!',
                'Surat Keluar berhasil diApprove',
                'success'
                )
            location.assign("/surat_keluar/acc/"+id);
            }
            });
        });
        
        // Tolak Surat Keluar
        $("#table-datatable").on("click", ".btn-tolak", function(e) {
            e.preventDefault();
            const id = $(this).data("id");

            swal({
            title: 'Kamu yakin?',
            text: "ingin Tolak Surat Keluar ini?",
            icon: 'warning',
            buttons: true,
            }).then((result) => {
            if (result) {
                swal(
                'Berhasil!',
                'Surat Keluar berhasil ditolak',
                'success'
                )
            location.assign("/surat_keluar/tolak/"+id);
            }
            });
        });
    });
</script>
<script>
    $('.penduduk').hide();
    $('.keterangan_skck').hide();
    $('.keterangan_pindah').hide();
    // $('.keterangan').hide();
    $('.no_kk').hide();
    $('.alamat_pindah').hide();
    $('.jml_pengikut').hide();
    $('.nikah').hide();
    
    $('#jenis_surat_id').change(function () {
        var selected = $('#jenis_surat_id option:selected').text();
    
            if (selected == "SURAT PENGANTAR CATATAN KEPOLISIAN") {
                $('.penduduk, .keterangan_skck').show();
                $('.nikah, .keterangan_pindah, .no_kk, .alamat_pindah, .jml_pengikut').hide();
                $('#jenis').val('skck');
            }

            if (selected == "SURAT PENGANTAR PINDAH ANTAR KOTA/KABUPATEN/PROVINSI") {
                $('.penduduk, .keterangan_pindah, .no_kk, .alamat_pindah, .jml_pengikut').show();
                $('.nikah, .keterangan_skck').hide();
                $('#jenis').val('pindah');
            } 
            
            if (selected == "SURAT KETERANGAN BELUM MEMILIKI RUMAH") {
                $('.penduduk').show();
                $('.keterangan_skck, .keterangan_pindah, .no_kk, .alamat_pindah, .jml_pengikut, .nikah').hide();
                $('#jenis').val('blm_pny_rmh');
            }
            
            if (selected == "SURAT NIKAH") {
                $('.penduduk, .nikah').show();
                $('.keterangan_skck, .keterangan_pindah, .no_kk, .alamat_pindah, .jml_pengikut').hide();
                $('#jenis').val('nikah');
            }
    });

</script>
@endpush