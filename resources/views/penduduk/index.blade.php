@extends ('layouts.base')
@section('title', 'Penduduk')
@section('content')

  {{-- content start --}}
  <div class="page-header">
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Penduduk</a>
        </li>
    </ul>
</div>
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h4 class="card-title">Penduduk</h4>
                    <button class="btn btn-primary btn-round btn-sm ml-auto add-btn" data-toggle="modal" data-target="#modal_penduduk">
                        <i class="fa fa-plus"></i>
                        Tambah
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-datatable" class="table table-striped custom-table w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>Jenis Kelamin</th>
                                <th>Alamat</th>
                                <th width="150" class="text-center">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- content end --}}

{{-- modal start --}}

{{-- Penduduk Modal start --}}

<div id="modal_penduduk" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>Tambah</span> Penduduk</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('penduduk.store') }}">
                    @csrf
                    <input type="hidden" id="id" name="id">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">NIK<span class="text-danger">*</span></label>
                            <input class="form-control" id="nik" name="nik" type="number" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Nama<span class="text-danger">*</span></label>
                            <input class="form-control" id="nama" name="nama" type="text" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Tempat Lahir<span class="text-danger">*</span></label>
                            <input class="form-control" id="tempat_lahir" name="tempat_lahir" type="text" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Tanggal Lahir<span class="text-danger">*</span></label>
                            <input class="form-control" id="tgl_lahir" name="tgl_lahir" type="date" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Jenis Kelamin<span class="text-danger">*</span></label>
                            <select name="jenis_kelamin" class="form-control">
                                <option value="">- Pilih -</option>
                                <option value="Laki - Laki">Laki - Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Alamat<span class="text-danger">*</span></label>
                            <input class="form-control" id="alamat" name="alamat" type="text" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Agama<span class="text-danger">*</span></label>
                            <select name="agama" class="form-control">
                                <option value="">- Pilih -</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen Protestan">Kristen Protestan</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Buddha">Buddha</option>
                                <option value="Konghucu">Konghucu</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Status Perkawinan<span class="text-danger">*</span></label>
                            <select name="status_perkawinan" class="form-control">
                                <option value="">- Pilih -</option>
                                <option value="Belum Kawin">Belum Kawin</option>
                                <option value="Menikah">Kawin</option>
                                <option value="Cerai">Cerai Hidup</option>
                                <option value="Cerai Mati">Cerai Mati</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Pekerjaan<span class="text-danger">*</span></label>
                            <input class="form-control" id="pekerjaan" name="pekerjaan" type="text" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-form-label">Kewarganegaraan<span class="text-danger">*</span></label>
                            <input class="form-control" id="kewarganegaraan" name="kewarganegaraan" type="text" required>
                        </div>
                    </div>
                </div>
                    <div class="modal-footer no-bd">
                        <button class="btn btn-primary submit-btn">Simpan</button>
                        <button class="btn btn-danger" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Penduduk Modal end --}}

{{--  Penduduk Detail Modal start --}}
<div id="modal_penduduk_detail" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>Detail</span> Penduduk</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped table-border">
                            <tbody>
                                <tr>
                                    <td>NIK :</td>
                                    <td class="text-right" id="nik"></td>
                                </tr>
                                <tr>
                                    <td>Nama :</td>
                                    <td class="text-right" id="nama"></td>
                                </tr>
                                <tr>
                                    <td>Tempat Lahir :</td>
                                    <td class="text-right" id="tempat_lahir"></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Lahir :</td>
                                    <td class="text-right" id="tgl_lahir"></td>
                                </tr>
                                <tr>
                                    <td>Jenis Kelamin :</td>
                                    <td class="text-right" id="jenis_kelamin"></td>
                                </tr>
                                <tr>
                                    <td>Alamat :</td>
                                    <td class="text-right" id="alamat"></td>
                                </tr>
                                <tr>
                                    <td>Agama :</td>
                                    <td class="text-right" id="agama"></td>
                                </tr>
                                <tr>
                                    <td>Status Perkawinan :</td>
                                    <td class="text-right" id="status_perkawinan"></td>
                                </tr>
                                <tr>
                                    <td>Kewarganegaraan :</td>
                                    <td class="text-right" id="kewarganegaraan"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--  Penduduk Detail Modal end --}}


{{-- Delete Penduduk Modal start --}}

<div class="modal custom-modal fade" id="delete_penduduk" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLongTitle">Hapus</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <h4 class="text-center">Anda yakin ingin hapus?</h4>
                <div class="modal-btn delete-action">
                    <div class="row">
                        <div class="col-6">
                            <form method="POST" id="url-delete">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-primary continue-btn w-100" type="submit">Hapus</button>
                            </form>
                        </div>
                        <div class="col-6">
                            <button type="button" data-dismiss="modal" class="btn btn-primary">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Delete Penduduk Modal end --}}

{{-- modal end --}}
@endsection

@push ('page-scripts')
<script>
    $(document).ready(function() {
        var table = $("#table-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('penduduk.json') }}",
            columns: [
                {
                    data: "id",
                    name: "id",
                },
                {
                    data: "nik",
                    name: "nik",
                },
                {
                    data: "nama",
                    name: "nama",
                },
                {
                    data: "tempat_lahir",
                    name: "tempat_lahir"
                },
                {
                    data: "tgl_lahir",
                    name: "tgl_lahir"
                },
                {
                    data: "jenis_kelamin",
                    name: "jenis_kelamin"
                },
                {
                    data: "alamat",
                    name: "alamat"
                },
                {
                    data: "action",
                    name: "action",
                    className: 'text-center'
                },
            ],
            order: [[ 0, "desc" ]]
        });
        table.on('draw.dt', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied', order: 'applied', page: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });

        // add penduduk
        $('.add-btn').on('click', function(){            
            $('#modal_penduduk .modal-title span').html('Tambah');
            $('#modal_penduduk #id').val('');
            $('#modal_penduduk form').trigger('reset');


            var myModal = new bootstrap.Modal(document.getElementById('modal_penduduk'))
            myModal.show()
        });

        // edit penduduk
        $("#table-datatable").on("click", ".btn-edit", function(e) {
            e.preventDefault();
            data = $(this).data();

            $.each(data, function(index, value){
                $('#modal_penduduk #'+index).val(value);
            })

            $('#modal_penduduk'+' .modal-title span').html('Edit');

            var myModal = new bootstrap.Modal(document.getElementById('modal_penduduk'))
            myModal.show()
        });

        // detail penduduk
        $("#table-datatable").on("click", ".btn-detail", function(e) {
            e.preventDefault();
            data = $(this).data();
    
            $.each(data, function(index, value){
                $('#modal_penduduk_detail #'+index).html(value);
            })
    
            var myModal = new bootstrap.Modal(document.getElementById('modal_penduduk_detail'))
            myModal.show()
        });


        // delete penduduk
        $("#table-datatable").on("click", ".btn-delete", function(e) {
            e.preventDefault();
            const id = $(this).data("id");
            $('#delete_penduduk #url-delete').attr('action', "{{ url('penduduk') }}/"+id);

    swal({
        title: 'Kamu Yakin Mau Hapus?',
        text: 'Jika di Hapus, Data akan hilang!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        swal('Data Berhasil diHapus :)', {
          icon: 'success',
        });
        $(`#url-delete`).submit();
        } else {
        swal('Data Kamu Aman!');
        }
      });
            // var myModal = new bootstrap.Modal(document.getElementById('delete_penduduk'))
            // myModal.show()
        });

    });
</script>
@endpush