<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Laporan Surat Keluar</title>
</head>
<body>
    <div class="form-group">
        <p align="center"><b>Laporan Surat Keluar</b></p>
        <p align="center"><b>Kelurahan Nusa Jaya</b></p>
        <p align="center"><b>Kota Tangerang</b></p>
        <table clas="static" align="center" rules="all" border="1px" style="width: 95%;">
            @foreach ($surat_keluar as $item)
            <tr>
                <th>Nomor Surat</th>
                <th>Jenis Surat</th>
                <th>Tanggal</th>
                <th>Perihal</th>
                <th>Foto Berkas</th>
            </tr>
            </thead>
            <tbody>             
              <tr>
                <td>{{ $item->no_surat }}</td>
                <td>{{ $item->jenis_surat->nama_surat ?? null}}</td>
                <td>{{ \Carbon\Carbon::create($item->tanggal)->translatedFormat('d F Y')}}</td>
                <td>{{ $item->perihal}}</td>
            </tr>
            @endforeach
            
        </table>
    </div>
</body>
</html>
