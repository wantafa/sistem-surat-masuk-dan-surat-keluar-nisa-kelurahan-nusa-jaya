<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Laporan Disposisi</title>
</head>
<body>
    <div class="form-group">
        <p align="center"><b>Laporan Disposisi</b></p>
        <p align="center"><b>Kelurahan Nusa Jaya</b></p>
        <p align="center"><b>Kota Tangerang</b></p>
        <table clas="static" align="center" rules="all" border="1px" style="width: 95%;">
            @foreach ($disposisi as $item)
            <tr>
                <th>Nomor Surat</th>
                <th>Jenis Surat</th>
                <th>Perihal</th>
                <th>Asal</th>
                <th>Intruksi</th>
                <th>Diteruskan</th>
                <th>Tanggal Penyelesaian</th>
                <th>Foto Berkas</th>
            </tr>
            </thead>
            <tbody>             
              <tr>
                <td>{{ $item->no_surat }}</td>
                <td>{{ $item->nama_surat}}</td>
                <td>{{ $item->perihal}}</td>
                <td>{{ $item->asal}}</td>
                <td>{{ $item->intruksi}}</td>
                <td>{{ $item->diteruskan}}</td>
                <td>{{ \Carbon\Carbon::create($item->tgl_penyelesaian)->translatedFormat('d F Y')}}</td>
                <td>
                    <img src="/uploads/images/foto_berkas/" width="100" height="100">
                </td>
            </tr>
            @endforeach
            
        </table>
    </div>
</body>
</html>
