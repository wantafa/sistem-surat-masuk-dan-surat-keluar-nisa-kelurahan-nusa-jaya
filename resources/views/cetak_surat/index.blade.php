@extends ('layouts.base')
@section('title', 'Cetak Surat')
@section('content')

  {{-- content start --}}
  <div class="page-header">
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Cetak Surat</a>
        </li>
    </ul>
</div>
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h4 class="card-title">Cetak Surat</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-datatable" class="table table-striped custom-table w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Nomor Surat Keluar</th>
                                <th>Jenis Surat Keluar</th>
                                <th class="text-center">SKCK</th>
                                <th class="text-center">Pindah Rumah</th>
                                <th class="text-center">Belum Punya Rumah</th>
                                <th class="text-center" width="140">Nikah</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- content end --}}

{{-- modal start --}}



{{-- modal end --}}
@endsection
@push ('page-scripts')
<script>
    $(document).ready(function() {
        var table = $("#table-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('cetak_surat.json') }}",
            columns: [
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                },
                {
                    data: "nama",
                    name: "nama",
                },
                {
                    data: "no_surat",
                    name: "no_surat"
                },
                {
                    data: "nama_surat",
                    name: "nama_surat"
                },
                {
                    data: "skck",
                    name: "skck",
                    className: 'text-center'
                },
                {
                    data: "pindah_rumah",
                    name: "pindah_rumah",
                    className: 'text-center'
                },
                {
                    data: "blm_pny_rmh",
                    name: "blm_pny_rmh",
                    className: 'text-center'
                },
                {
                    data: "nikah",
                    name: "nikah",
                    className: 'text-center'
                },
            ],
            order: [[ 0, "desc" ]]
        });
        table.on('draw.dt', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied', order: 'applied', page: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });

        // add Cetak Surat
        $('.add-btn').on('click', function(){            
            $('#modal_surat_masuk .modal-title span').html('Tambah');
            $('#modal_surat_masuk #id').val('');
            $('#modal_surat_masuk form').trigger('reset');


            var myModal = new bootstrap.Modal(document.getElementById('modal_surat_masuk'))
            myModal.show()
        });

        // edit Cetak Surat
        $("#table-datatable").on("click", ".btn-edit", function(e) {
            e.preventDefault();
            data = $(this).data();

            $.each(data, function(index, value){
                $('#modal_surat_masuk #'+index).val(value);
            })

            $('#modal_surat_masuk'+' .modal-title span').html('Edit');

            var myModal = new bootstrap.Modal(document.getElementById('modal_surat_masuk'))
            myModal.show()
        });

        // detail Cetak Surat
        $("#table-datatable").on("click", ".btn-detail", function(e) {
            e.preventDefault();
            data = $(this).data();
    
            $.each(data, function(index, value){
                $('#modal_surat_masuk_detail #'+index).html(value);
            })
    
            var myModal = new bootstrap.Modal(document.getElementById('modal_surat_masuk_detail'))
            myModal.show()
        });

        // delete Cetak Surat
        $("#table-datatable").on("click", ".btn-delete", function(e) {
            e.preventDefault();
            const id = $(this).data("id");
            $('#delete_surat_masuk #url-delete').attr('action', "{{ url('surat_masuk') }}/"+id);

    swal({
        title: 'Kamu Yakin Mau Hapus?',
        text: 'Jika di Hapus, Data akan hilang!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        swal('Data Berhasil diHapus :)', {
          icon: 'success',
        });
        $(`#url-delete`).submit();
        } else {
        swal('Data Kamu Aman!');
        }
      });
            // var myModal = new bootstrap.Modal(document.getElementById('delete_surat_masuk'))
            // myModal.show()
        });

    });
</script>
@endpush