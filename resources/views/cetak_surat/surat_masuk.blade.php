@extends ('layouts.base')
@section('title', 'Cetak Surat')
@section('content')

  {{-- content start --}}
  <div class="page-header">
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Cetak Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Laporan Surat Masuk</a>
        </li>
    </ul>
</div>
      <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row container">
                        <div class="card-body">
                            <form action="{{ route('report.surat_masuk') }}" method="GET" class="form-group" id="formFilter">
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="label">Tanggal Masuk</label>
                                        <input type="date" name="tgl_masuk" id="tgl_masuk" class="form-control datepicker border-primary"/> 
                                    </div>
                                    <div class="col-md-4">
                                        <label for="label">Perihal</label>
                                        <input type="text" name="perihal" id="perihal" class="form-control border-primary"/> 
                                    </div>
                                    <div class="col-md-4">
                                        <label for="label">Jenis Surat</label>
                                        <select name="jenis_surat" class="form-control border-primary">
                                            @foreach ($get_jenis as $item)
                                            <option value="{{ $item->id }}">{{ $item->nama_surat }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </form>
                            <div class="d-flex justify-content-center">
                                <button class="btn btn-info btn-block mt-2" type="submit" form="formFilter" value="Submit">Cari Data</button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
      </div>

  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h4 class="card-title">Cetak Surat Masuk</h4>
                    <a href="{{ route('report.pdf_surat_masuk', csrf_token())  }}&tgl_masuk={{ $tgl_masuk }}&perihal={{ $perihal }}&jenis_surat={{ $jenis_surat }}" class="btn btn-success btn-round ml-auto"><i class="fa fa-print"></i> Cetak</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-datatable" class="table table-striped custom-table w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Nomor Surat</th>
                                <th>Perihal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cari as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->tanggal }}</td>
                                    <td>{{ $item->no_surat }}</td>
                                    <td>{{ $item->perihal }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- content end --}}

{{-- modal start --}}

{{-- Cetak Surat Modal start --}}


{{-- modal end --}}
@endsection
@push ('page-scripts')
@endpush