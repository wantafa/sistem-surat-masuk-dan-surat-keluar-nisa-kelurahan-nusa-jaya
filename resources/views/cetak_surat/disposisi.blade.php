@extends ('layouts.base')
@section('title', 'Cetak Surat')
@section('content')

  {{-- content start --}}
  <div class="page-header">
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Cetak Surat</a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="#">Laporan Disposisi</a>
        </li>
    </ul>
</div>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="row container">
            <div class="card-body">
              <form action="{{ route('report.disposisi') }}" method="GET" class="form-group" id="formFilter">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <label for="label">Tanggal Masuk</label>
                            <input type="date" name="tgl_masuk" id="tgl_masuk" class="form-control datepicker border-primary"/> 
                        </div>
                        <div class="col-md-4">
                            <label for="label">Asal</label>
                            <input type="date" name="asal" id="asal" class="form-control border-primary"/> 
                        </div>
                        <div class="col-md-4">
                            <label for="label">Perihal</label>
                            <input type="date" name="perihal" id="perihal" class="form-control border-primary"/> 
                        </div>
                    </div>
                <div class="d-flex justify-content-center">
                        <button class="btn btn-info btn-block mt-2" type="submit" form="formFilter" value="Submit">Cari Data</button>
                    </div>
              </form>
          </div>
        </div>
        </div>
      </div>
      </div>

  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h4 class="card-title">Cetak Disposisi</h4>
                    <a href="{{ route('report.pdf_disposisi', csrf_token())  }}&tgl_disposisi={{ $tgl_disposisi }}" class="btn btn-success btn-round ml-auto"><i class="fa fa-print"></i> Cetak</a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="table-datatable" class="table table-striped custom-table w-100">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Asal</th>
                                <th>Intruksi</th>
                                <th>Diteruskan</th>
                                <th>Tanggal Penyelesaian</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cari as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->asal }}</td>
                                    <td>{{ $item->intruksi }}</td>
                                    <td>{{ $item->diteruskan }}</td>
                                    <td>{{ $item->tgl_penyelesaian }}</td>
                                    <td>{{ $item->status }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- content end --}}

{{-- modal start --}}

{{-- Cetak Surat Modal start --}}


{{-- modal end --}}
@endsection
@push ('page-scripts')
@endpush