<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SKCK </title>
</head>
<style>
    .center {
        text-align: center;
    }

    .tbl-center {
        margin-left: 50px;
        margin-right: auto;
    }
</style>
<body>

    <img src="{{ public_path('assets/img/kop.jpg') }}" height="15%" width="100%">
    <div style="text-align: center;font-size:18px;margin-top:20px;margin-bottom:20px;">
        <span><strong> <u>SURAT PENGANTAR  CATATAN KEPOLISIAN</u></strong></span><br>
        <span class="center">{{ $skck->no_surat }}</span>
    </div>


    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan dibawah ini Lurah Nusa Jaya, Kecamatan Karawaci Kota Tangerang, menerangkan bahwa dengan sebenarnya bahwa :</p>

    <table class="tbl-center">
        <tr>
            <td>Nama</td>
            <td> :</td>
            <td> {{ $skck->nama }}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td> :</td>
            <td> {{ $skck->jenis_kelamin }}</td>
        </tr>        
        <tr>
            <td width="200px">Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $skck->tempat_lahir }}, {{ \Carbon\Carbon::create($skck->tgl_lahir)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td>Agama</td>
            <td> :</td>
            <td> {{ $skck->agama }}</td>
        </tr>        
        <tr>
            <td>KTP Lapor Diri No</td>
            <td> :</td>
            <td> {{ $skck->nik }}</td>
        </tr>        
        <tr>
            <td>Alamat</td>
            <td> :</td>
            <td> {{ $skck->alamat }}</td>
        </tr>        
    </table>

    <p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Keterangan : Nama dan alamat diatas benar warga kami dan menurut sepengetahuan kami serta pengantar dari RT / RW setempat yang bersangkutan belum pernah berurusan dengan Polisi / Berkelakuan baik. Surat keterangan ini diperlukan untuk mengurus kelakuan baik di kepolisian.</p>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian surat keterangan ini dibuat dengan sebenarnya, dan kepada yang berkepentingan agar maklum dan dapat memberikan bantuan seperlunya.</p>

    <div style="float: right; margin-top: 20px">
        <div style="text-align: center;">
            <span>Tangerang, {{ \Carbon\Carbon::create($skck->tanggal)->translatedFormat('d F Y') }}</span><br>
            <p style="margin-top: 20px">LURAH NUSA JAYA</p>

        </div>
    </div>

</body>
</html>