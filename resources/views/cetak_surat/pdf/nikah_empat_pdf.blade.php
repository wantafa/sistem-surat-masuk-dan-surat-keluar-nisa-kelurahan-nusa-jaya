<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PERSETUJUAN CALON PENGANTIN </title>
</head>
<style>
    .center {
        text-align: center;
    }

    .tbl-center {
        margin-left: 20px;
        margin-right: auto;
    }
</style>
<body>
    <img src="{{ public_path('assets/img/kop.jpg') }}" height="15%" width="100%">
    <span>Lampiran VIII</span><br>
    <span>Keputusan Direktur Jendral Bimbingan Masyarakat Islam</span><br>
    <span>Nomor: 473 Tahun 2020</span><br>
    <span>Tentang</span><br>
    <span>Petunjuk Teknis Pelaksanaan Pencatatan Pernikahan</span><br>
    <p style="float: right;">Model N4</p><br>
    
    <div style="text-align: center;font-size:18px;margin-top:50px;margin-bottom:20px;">
        <span><strong> <u>PERSETUJUAN CALON PENGANTIN</u></strong></span><br>
    </div>


    <p>Yang bertanda tangan dibawah ini :</p>
    <p>A. Calon Suami :</p>

    <table class="tbl-center">
        <tr>
            <td>1.</td>
            <td>Nama Lengkap dan Alias</td>
            <td> :</td>
            <td> {{ $calon_suami->nama }}</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Bin</td>
            <td> :</td>
            <td> .......</td>
        </tr>        
        <tr>
            <td>3.</td>
            <td>Nomor Induk Kependudukan</td>
            <td> :</td>
            <td> {{ $calon_suami->nik }}</td>
        </tr>        
        <tr>
            <td>4.</td>
            <td>Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $calon_suami->tempat_lahir }}, {{ \Carbon\Carbon::create($calon_suami->tgl_lahir)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td>5.</td>
            <td>Kewarganegaraan</td>
            <td> :</td>
            <td> {{ $calon_suami->kewarganegaraan }}</td>
        </tr>        
        <tr>
            <td>6.</td>
            <td>Pekerjaan</td>
            <td> :</td>
            <td> {{ $calon_suami->pekerjaan }}</td>
        </tr>     
        <tr>
            <td>7.</td>
            <td>Alamat</td>
            <td> :</td>
            <td> {{ $calon_suami->alamat }}</td>
        </tr>        
    </table>
        <p>B. Calon Istri</p>
    <table class="tbl-center">
        <tr>
            <td>1.</td>
            <td>Nama Lengkap dan Alias</td>
            <td> :</td>
            <td> {{ $calon_istri->nama }}</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Bin</td>
            <td> :</td>
            <td> .......</td>
        </tr>        
        <tr>
            <td>3.</td>
            <td>Nomor Induk Kependudukan</td>
            <td> :</td>
            <td> {{ $calon_istri->nik }}</td>
        </tr>        
        <tr>
            <td>4.</td>
            <td>Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $calon_istri->tempat_lahir }}, {{ \Carbon\Carbon::create($calon_istri->tgl_lahir)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td>5.</td>
            <td>Kewarganegaraan</td>
            <td> :</td>
            <td> {{ $calon_istri->kewarganegaraan }}</td>
        </tr>        
        <tr>
            <td>6.</td>
            <td>Pekerjaan</td>
            <td> :</td>
            <td> {{ $calon_istri->pekerjaan }}</td>
        </tr>     
        <tr>
            <td>7.</td>
            <td>Alamat</td>
            <td> :</td>
            <td> {{ $calon_istri->alamat }}</td>
        </tr>        
    </table>

    <p style="text-align: justify">Menyatakan dengan sesungguhnya bahwa atas dasar sukarela, dengan kesadaran sendiri, tanpa ada paksaan dari siapapun juga, setuju untuk melangsungkan pernikahan.</p>
    
    <p>Demikian surat persetujuan ini dibuat untuk dapat digunakan seperlunya.</p>
    

    <table>
        <tr>
            <td style="padding-left: 12px;">
                <div style="text-align: center;">
                    <span>Calon Suami</span>
                    <p style="margin-top: 80px"> {{ $calon_suami->nama }}</p>
                </div>
            </td>
            <td style="padding-right: 300px;">   </td>
            <td>
                <div style="text-align: center">
                    <span>Calon Istri</span><br>
                    <p style="margin-top: 80px">{{ $calon_istri->nama }}</p>
                </div>
            </td>
        </tr>
    </table>

</body>
</html>