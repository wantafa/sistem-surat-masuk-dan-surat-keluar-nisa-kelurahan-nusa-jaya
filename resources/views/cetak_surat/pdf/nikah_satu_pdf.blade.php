<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PENGANTAR NIKAH </title>
</head>
<style>
    .center {
        text-align: center;
    }

    .tbl-center {
        margin-left: 20px;
        margin-right: auto;
    }
</style>
<body>
    <img src="{{ public_path('assets/img/kop.jpg') }}" height="15%" width="100%">
    <div style="padding-left: 4px; padding-top: 2px; padding-right: 4px">
    <span>Lampiran V</span><br>
    <span>Keputusan Direktur Jendral Bimbingan Masyarakat Islam</span><br>
    <span>Nomor: 473 Tahun 2020</span><br>
    <span>Tentang</span><br>
    <span>Petunjuk Teknis Pelaksanaan Pencatatan Pernikahan</span><br>
    <p style="float: right;">Model N1</p><br><br>

    <table>
        <tr>
            <td>KANTOR DESA/KELURAHAN</td>
            <td>:</td>
            <td>NUSA JAYA</td>
        </tr>
        <tr>
            <td>KECAMATAN</td>
            <td>:</td>
            <td>KARAWACI</td>
        </tr>
        <tr>
            <td>KABUPATEN/KOTA</td>
            <td>:</td>
            <td>TANGERANG</td>
        </tr>
    </table>
    
    <div style="text-align: center;font-size:18px;margin-top:50px;margin-bottom:20px;">
        <span><strong> <u>PENGANTAR NIKAH</u></strong></span><br>
        <span class="center">{{ $nikah->no_surat }}</span>
    </div>


    <p>Yang bertanda tangan dibawah ini menerangkan dengan sesungguhnya bahwa :</p>

    <table class="tbl-center">
        <tr>
            <td>1.</td>
            <td>Nama Lengkap dan Alias</td>
            <td> :</td>
            <td> {{ $calon->nama }}</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Nomor Induk Kependudukan</td>
            <td> :</td>
            <td> {{ $calon->nik }}</td>
        </tr>        
        <tr>
            <td>3.</td>
            <td>Jenis Kelamin</td>
            <td> :</td>
            <td> {{ $calon->jenis_kelamin }}</td>
        </tr>        
        <tr>
            <td>4.</td>
            <td>Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $calon->tempat_lahir }}, {{ \Carbon\Carbon::create($calon->tgl_lahir)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td>5.</td>
            <td>Kewarganegaraan</td>
            <td> :</td>
            <td> {{ $calon->kewarganegaraan }}</td>
        </tr>        
        <tr>
            <td>6.</td>
            <td>Agama</td>
            <td> :</td>
            <td> {{ $calon->agama }}</td>
        </tr>        
        <tr>
            <td>7.</td>
            <td>Pekerjaan</td>
            <td> :</td>
            <td> {{ $calon->pekerjaan }}</td>
        </tr>
        <tr>
            <td>8.</td>
            <td>Alamat</td>
            <td> :</td>
            <td> {{ $calon->alamat }}</td>
        </tr>        
        <tr>     
            <td>9.</td>
            <td>Bin / Binti</td>
            <td> :</td>
            <td> {{ $ayah->nama }}</td>
        </tr>
        <tr>
            <td colspan="2">ott Status Perkawinan</td>
        </tr>
            <tr>
                <td></td>
                <td>a. Laki - Laki : Jejaka, Duda, atau beristri <br> ke....</td>
                <td>:</td>
                <td>........................</td>
            </tr>
            <tr>
                <td></td>
                <td>b. Perempuan : Perawan, Janda</td>
                <td>:</td>
                <td>........................</td>
            </tr>
            <tr>     
                <td>10.</td>
                <td>Nama Istri / suami terdahulu</td>
                <td> :</td>
                <td> ........................</td>
            </tr>
        <tr>
            <td colspan="2">Adalah benar anak dari perkawinan seorang pria :</td>
        </tr>
        <tr>
            <td>1.</td>
            <td>Nama Lengkap dan Alias</td>
            <td> :</td>
            <td> {{ $ayah->nama }}</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Nomor Induk Kependudukan</td>
            <td> :</td>
            <td> {{ $ayah->nik }}</td>
        </tr>        
        <tr>
            <td>3.</td>
            <td>Jenis Kelamin</td>
            <td> :</td>
            <td> {{ $ayah->jenis_kelamin }}</td>
        </tr>        
        <tr>
            <td>4.</td>
            <td>Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $ayah->tempat_lahir }}, {{ \Carbon\Carbon::create($ayah->tgl_lahir)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td>5.</td>
            <td>Kewarganegaraan</td>
            <td> :</td>
            <td> {{ $ayah->kewarganegaraan }}</td>
        </tr>        
        <tr>
            <td>6.</td>
            <td>Agama</td>
            <td> :</td>
            <td> {{ $ayah->agama }}</td>
        </tr>        
        <tr>
            <td>7.</td>
            <td>Pekerjaan</td>
            <td> :</td>
            <td> {{ $ayah->pekerjaan }}</td>
        </tr>
        <tr>
            <td>8.</td>
            <td>Alamat</td>
            <td> :</td>
            <td> {{ $ayah->alamat }}</td>
        </tr> 
        <tr>
            <td colspan="2">Dengan seorang wanita :</td>
        </tr>
        <tr>
            <td>1.</td>
            <td>Nama Lengkap dan Alias</td>
            <td> :</td>
            <td> {{ $ibu->nama }}</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Nomor Induk Kependudukan</td>
            <td> :</td>
            <td> {{ $ibu->nik }}</td>
        </tr>        
        <tr>
            <td>3.</td>
            <td>Jenis Kelamin</td>
            <td> :</td>
            <td> {{ $ibu->jenis_kelamin }}</td>
        </tr>        
        <tr>
            <td>4.</td>
            <td>Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $ibu->tempat_lahir }}, {{ Carbon\Carbon::create($ibu->tgl_lahir)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td>5.</td>
            <td>Kewarganegaraan</td>
            <td> :</td>
            <td> {{ $ibu->kewarganegaraan }}</td>
        </tr>        
        <tr>
            <td>6.</td>
            <td>Agama</td>
            <td> :</td>
            <td> {{ $ibu->agama }}</td>
        </tr>        
        <tr>
            <td>7.</td>
            <td>Pekerjaan</td>
            <td> :</td>
            <td> {{ $ibu->pekerjaan }}</td>
        </tr>
        <tr>
            <td>8.</td>
            <td>Alamat</td>
            <td> :</td>
            <td> {{ $ibu->alamat }}</td>
        </tr>
    </table>

    <p>Demikian surat pengantar ini dibuat dengan mengingat sumpah jabatan dan untuk dipergunakan sebagaimana mestinya.</p>
    

    <div style="float: right; margin-top: 20px">
        <div style="text-align: center;">
            <span>Tangerang, {{ \Carbon\Carbon::create($nikah->tanggal)->translatedFormat('d F Y') }}</span><br>
            <p style="margin-top: 20px">a.n Kepala Desa / Lurah.......................</p>

        </div>
    </div>
</div>
</body>
</html>