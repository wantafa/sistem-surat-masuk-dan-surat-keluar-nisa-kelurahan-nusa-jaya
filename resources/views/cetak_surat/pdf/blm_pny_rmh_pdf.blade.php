<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SURAT BELUM PUNYA RUMAH </title>
</head>
<style>
    .center {
        text-align: center;
    }

    .tbl-center {
        margin-left: 50px;
        margin-right: auto;
    }
</style>
<body>
    <img src="{{ public_path('assets/img/kop.jpg') }}" height="15%" width="100%">
    <div style="text-align: center;font-size:18px;margin-top:20px;margin-bottom:20px;">
        <span><strong> <u>SURAT KETERANGAN BELUM MEMILIKI RUMAH</u></strong></span><br>
        <span class="center">{{ $blm_pny_rmh->no_surat }}</span>
    </div>


    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan dibawah ini Lurah Nusa Jaya, Kecamatan Karawaci Kota Tangerang, menerangkan bahwa dengan sebenarnya bahwa :</p>

    <table class="tbl-center">
        <tr>
            <td>Nama Lengkap</td>
            <td> :</td>
            <td> {{ $blm_pny_rmh->nama }}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td> :</td>
            <td> {{ $blm_pny_rmh->jenis_kelamin }}</td>
        </tr>        
        <tr>
            <td>Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $blm_pny_rmh->tempat_lahir }}, {{ $blm_pny_rmh->tgl_lahir }}</td>
        </tr>        
        <tr>
            <td>Kewarganegaraan</td>
            <td> :</td>
            <td> {{ $blm_pny_rmh->kewarganegaraan }}</td>
        </tr>        
        <tr>
            <td>Agama</td>
            <td> :</td>
            <td> {{ $blm_pny_rmh->agama }}</td>
        </tr>        
        <tr>
            <td>Pekerjaan</td>
            <td> :</td>
            <td> {{ $blm_pny_rmh->pekerjaan }}</td>
        </tr>        
        <tr>
            <td>KTP Lapor Diri No</td>
            <td> :</td>
            <td> {{ $blm_pny_rmh->nik }}</td>
        </tr>        
        <tr>
            <td>Alamat</td>
            <td> :</td>
            <td> {{ $blm_pny_rmh->alamat }}</td>
        </tr>        
    </table>

    <p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bahwa nama tersebut diatas benar warga kami dan menurut keterangan dari RT/RW setempat serta data yang ada nama tersebut benar belum pernah memiliki rumah, surat keterangan ini untuk melengkapi persyaratan pengajuan subsidi KPR/BTN.</b> </p>
    

    <div style="float: right; margin-top: 20px">
        <div style="text-align: center;">
            <span>Tangerang, {{ \Carbon\Carbon::create($blm_pny_rmh->tanggal)->translatedFormat('d F Y') }}</span><br>
            <p style="margin-top: 20px">a.n LURAH NUSA JAYA</p>

        </div>
    </div>


</body>
</html>