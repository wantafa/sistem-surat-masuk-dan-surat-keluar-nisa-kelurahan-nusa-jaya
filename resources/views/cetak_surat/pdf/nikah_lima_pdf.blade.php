<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SURAT IZIN ORANG TUA </title>
</head>
<style>
    .center {
        text-align: center;
    }

    .tbl-center {
        margin-left: 20px;
    }
</style>
<body>
    <img src="{{ public_path('assets/img/kop.jpg') }}" height="15%" width="100%">
    <div style="padding: 20px">
    <span>Lampiran IX</span><br>
    <span>Keputusan Direktur Jendral Bimbingan Masyarakat Islam</span><br>
    <span>Nomor: 473 Tahun 2020</span><br>
    <span>Tentang</span><br>
    <span>Petunjuk Teknis Pelaksanaan Pencatatan Pernikahan</span><br>
    <p style="float: right;">Model N5</p><br><br>

    <div style="text-align: center;font-size:18px;margin-top:20px;margin-bottom:20px;">
        <span><strong> <u>SURAT IZIN ORANG TUA</u></strong></span><br>
        <span class="center">{{ $nikah->no_surat }}</span>
    </div>


    
    <table style="font-size: 15px;">
        <tr>
            <td colspan="5">Yang bertanda tangan dibawah ini :</td>
        </tr>
        <tr>
            <td width="10px">A.</td>
            <td width="40px">1.</td>
            <td width="220px">Nama Lengkap dan Alias</td>
            <td width="10px"> :</td>
            <td width="300px"> {{ $ayah_calon->nama }}</td>
        </tr>
        <tr>    
            <td></td>
            <td>2.</td>
            <td>Bin</td>
            <td> :</td>
            <td> {{ $nikah->ortu_ayah }}</td>
        </tr>
        <tr>
            <td></td>
            <td>3.</td>
            <td>Nomor Induk Kependudukan</td>
            <td> :</td>
            <td> {{ $ayah_calon->nik }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>4.</td>
            <td>Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $ayah_calon->tempat_lahir }}, {{ \Carbon\Carbon::create($ayah_calon->tgl_lahir)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>5.</td>
            <td>Kewarganegaraan</td>
            <td> :</td>
            <td> {{ $ayah_calon->kewarganegaraan }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>6.</td>
            <td>Agama</td>
            <td> :</td>
            <td> {{ $ayah_calon->agama }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>7.</td>
            <td>Pekerjaan</td>
            <td> :</td>
            <td> {{ $ayah_calon->pekerjaan }}</td>
        </tr>
        <tr>
            <td></td>
            <td>8.</td>
            <td>Alamat</td>
            <td> :</td>
            <td> {{ $ayah_calon->alamat }}</td>
        </tr>  
        
        <tr>
            <td>B.</td>
            <td>1.</td>
            <td>Nama Lengkap dan Alias</td>
            <td> :</td>
            <td> {{ $ibu_calon->nama }}</td>
        </tr>
        <tr>    
            <td></td>
            <td>2.</td>
            <td>Binti</td>
            <td> :</td>
            <td> {{ $nikah->ortu_ibu }}</td>
        </tr>
        <tr>
            <td></td>
            <td>3.</td>
            <td>Nomor Induk Kependudukan</td>
            <td> :</td>
            <td> {{ $ibu_calon->nik }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>4.</td>
            <td>Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $ibu_calon->tempat_lahir }}, {{ \Carbon\Carbon::create($ibu_calon->tgl_lahir)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>5.</td>
            <td>Kewarganegaraan</td>
            <td> :</td>
            <td> {{ $ibu_calon->kewarganegaraan }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>6.</td>
            <td>Agama</td>
            <td> :</td>
            <td> {{ $ibu_calon->agama }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>7.</td>
            <td>Pekerjaan</td>
            <td> :</td>
            <td> {{ $ibu_calon->pekerjaan }}</td>
        </tr>
        <tr>
            <td></td>
            <td>8.</td>
            <td>Alamat</td>
            <td> :</td>
            <td> {{ $ibu_calon->alamat }}</td>
        </tr>  

        <tr>
            <td colspan="5">Adalah ayah dan ibu kandung/wali/pengampu dari:</td>
        </tr>
        
        <tr>
            <td></td>
            <td>1.</td>
            <td>Nama Lengkap dan Alias</td>
            <td> :</td>
            <td> {{ $calon_suami->nama }}</td>
        </tr>
        <tr>    
            <td></td>
            <td>2.</td>
            <td>Bin/Binti</td>
            <td> :</td>
            <td> {{ $ayah_calon->nama }}</td>
        </tr>
        <tr>
            <td></td>
            <td>3.</td>
            <td>Nomor Induk Kependudukan</td>
            <td> :</td>
            <td> {{ $calon_suami->nik }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>4.</td>
            <td>Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $calon_suami->tempat_lahir }}, {{ \Carbon\Carbon::create($calon_suami->tgl_lahir)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>5.</td>
            <td>Kewarganegaraan</td>
            <td> :</td>
            <td> {{ $calon_suami->kewarganegaraan }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>6.</td>
            <td>Agama</td>
            <td> :</td>
            <td> {{ $calon_suami->agama }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>7.</td>
            <td>Pekerjaan</td>
            <td> :</td>
            <td> {{ $calon_suami->pekerjaan }}</td>
        </tr>
        <tr>
            <td></td>
            <td>8.</td>
            <td>Alamat</td>
            <td> :</td>
            <td> {{ $calon_suami->alamat }}</td>
        </tr>  
        
        <tr>
            <td colspan="5">memberikan izin kepada anak Kami untuk melakukan pernikanan dengan:</td>
        </tr>
        
        <tr>
            <td></td>
            <td>1.</td>
            <td>Nama Lengkap dan Alias</td>
            <td> :</td>
            <td> {{ $calon_istri->nama }}</td>
        </tr>
        <tr>    
            <td></td>
            <td>2.</td>
            <td>Bin/Binti</td>
            <td> :</td>
            <td> {{ $nikah->ortu_calon }}</td>
        </tr>
        <tr>
            <td></td>
            <td>3.</td>
            <td>Nomor Induk Kependudukan</td>
            <td> :</td>
            <td> {{ $calon_istri->nik }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>4.</td>
            <td>Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $calon_istri->tempat_lahir }}, {{ \Carbon\Carbon::create($calon_istri->tgl_lahir)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td></td>
            <td>5.</td>
            <td>Kewarganegaraan</td>
            <td> :</td>
            <td> {{ $calon_istri->kewarganegaraan}}</td>
        </tr>        
        <tr>
            <td></td>
            <td>6.</td>
            <td>Agama</td>
            <td> :</td>
            <td> {{ $calon_istri->agama}}</td>
        </tr>        
        <tr>
            <td></td>
            <td>7.</td>
            <td>Pekerjaan</td>
            <td> :</td>
            <td> {{ $calon_istri->pekerjaan}}</td>
        </tr>
        <tr>
            <td></td>
            <td>8.</td>
            <td>Alamat</td>
            <td> :</td>
            <td> {{ $calon_istri->alamat}}</td>
        </tr>  
    </table>

    <p>Demikian surat izin ini dibuat dengan kesadaran tanpa ada paksaan dari siapapun dan untuk digunakan seperlunya.</p>
    

    <table>
        <tr>
            <td style="padding-left: 12px;">
                <div style="text-align: center;">
                    <span>Ayah/wali/pengampu</span>
                    <p style="margin-top: 80px"> {{ $ayah_calon->nama}}</p>
                </div>
            </td>
            <td style="padding-right: 280px;">   </td>
            <td>
                <div>
                    <span>Tangerang, {{ \Carbon\Carbon::create($nikah->tanggal)->translatedFormat('d F Y') }}</span><br>
                    <span>Ibu/wali/pengampu</span><br>
                    <p style="margin-top: 80px">{{ $ibu_calon->nama}}</p>
                </div>
            </td>
        </tr>
    </table>

</body>
</div>
</html>