<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SURAT PINDAH RUMAH </title>
</head>
<style>
    .center {
        text-align: center;
    }

    .tbl-center {
        margin-left: 50px;
        margin-right: auto;
    }
</style>
<body>
    <img src="{{ public_path('assets/img/kop.jpg') }}" height="15%" width="100%">
    <div style="text-align: center;font-size:18px;margin-top:20px;margin-bottom:20px;">
        <span><strong> <u>SURAT PENGANTAR  PINDAH ANTAR KOTA/KABUPATEN/PROVINSI</u></strong></span><br>
        <span class="center" >{{ $pindah_rumah->no_surat }}</span>
    </div>


    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Yang bertanda tangan dibawah ini Lurah Nusa Jaya, Kecamatan Karawaci Kota Tangerang, menerangkan bahwa dengan sebenarnya bahwa :</p>

    <table class="tbl-center">
        <tr>
            <td>1.</td>
            <td>Nama Lengkap</td>
            <td> :</td>
            <td> {{ $pindah_rumah->nama }}</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Nomor Induk Kependudukan</td>
            <td> :</td>
            <td> {{ $pindah_rumah->nik }}</td>
        </tr>        
        <tr>
            <td>3.</td>
            <td>Nomor Kartu Keluarga</td>
            <td> :</td>
            <td> {{ $pindah_rumah->no_kartu_keluarga }}</td>
        </tr>        
        <tr>
            <td>4.</td>
            <td>Jenis Kelamin</td>
            <td> :</td>
            <td> {{ $pindah_rumah->jenis_kelamin }}</td>
        </tr>        
        <tr>
            <td>5.</td>
            <td>Tempat Tanggal Lahir</td>
            <td> :</td>
            <td> {{ $pindah_rumah->tempat_lahir }}, {{ \Carbon\Carbon::create($pindah_rumah->tgl_lahir)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td>6.</td>
            <td>Status Perkawinan</td>
            <td> :</td>
            <td> {{ $pindah_rumah->status_perkawinan }}</td>
        </tr>        
        <tr>
            <td>7.</td>
            <td>Alamat Terakhir</td>
            <td> :</td>
            <td> {{ $pindah_rumah->alamat }}</td>
        </tr>        
        <tr>
            <td>8.</td>
            <td>Pekerjaan</td>
            <td> :</td>
            <td> {{ $pindah_rumah->pekerjaan }}</td>
        </tr>        
        <tr>
            <td>9.</td>
            <td>Pindah ke</td>
            <td> :</td>
        <td> {{ $pindah_rumah->alamat_pindah }}</td>
        </tr>        
        <tr>
            <td>10.</td>
            <td>Jumlah Pengikut</td>
            <td> :</td>
            <td> {{ $pindah_rumah->jumlah_pengikut }}</td>
        </tr>        
    </table>

    <p style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Surat keterangan Pindah ini dibuat untuk keperluan pengurusan <b>Surat Keterangan Pindah antar Kota/Kabupaten/Propinsi.</b> </p>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Demikian surat keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</p>
    

    <table>
        <tr>
            <td style="padding-left: 12px;">
                <div style="text-align: center;">
                    <span>Pemohon</span>
                    <p style="margin-top: 80px"> {{ $pindah_rumah->nama }}</p>
                </div>
            </td>
            <td style="padding-right: 400px;">   </td>
            <td>
                <div>
                    <span>Tangerang, {{ \Carbon\Carbon::create($pindah_rumah->tanggal)->translatedFormat('d F Y') }}</span><br>
                    <span>an. LURAH NUSA JAYA</span>
                    <p style="margin-top: 80px"></p>
                </div>
            </td>
        </tr>
    </table>
    <div class="row">
        <div class="col-xs-12" style="text-align: center">
            <span>Nomor.........................</span><br>
            <span>  Pengesahan</span><br>
            <span>  Camat Karawaci</span>
        </div>
    </div>


</body>
</html>