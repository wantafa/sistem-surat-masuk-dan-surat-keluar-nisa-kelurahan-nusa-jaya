<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=	, initial-scale=1.0">
	<title>LOGIN - KELURAHAN NUSA JAYA</title>

	<!-- Bootstrap CSS -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700&display=swap" rel="stylesheet">

	<style>	
		body {
		    font-family: 'Poppins';
		    font-weight: 400;
		    font-size: 16px;
		    line-height: 2;
		    background: #DBD0BD;
		    /* color: #777; */
		    color: #4d4b4b;
		}
		.centerDiv {
			height: 100vh;
			width: 100%;
		}
        .card {background: #DBD0BD}
	</style>	
</head>
<body>
	<div class="container-fluid">	
		<div class="row centerDiv">
			<div class="col-sm-12 my-auto">
				<div class="card border-0">
				  <div class="row">
				    <div class="col-md-8">
				      <div class="card-body">
                        <h4 class="text-center">Sistem Layanan Administrasi Surat Pengantar dan Arsip Digital Kelurahan Nusa Jaya Kota Tangerang</h3>
				      	<img src="assets/img/bg-login.jpeg" class="img-fluid rounded-start shadow mt-5" alt="...">
				      </div>
				    </div>
				    <div class="col-md-4 border border-dark">
				      <div class="card-body">
				      	<div class="mb-4 text-center">
				      		<img src="assets/img/logo.png" class="img-fluid" width="100" height="100">
                        </div>
			      		<form action="{{ route('login') }}" method="post">
                            @csrf
			      		  <div class="mb-2">
                            <label class="label" for="name">Email</label>
                            <input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Masukkan Email" required>
                            @error('email')
                            <label id="email-error" class="error text-danger fw-bold" for="email">{{ $message }}</label>
                            @enderror
			      		  </div>
			      		  <div class="mb-5">
                            <label class="label" for="password">Password</label>
                            <input id="password" name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Masukkan Password" required>
                            @error('password')
                            <label id="password-error" class="error text-danger" for="password">{{ $message }}</label>
                            @enderror
			      		  </div>
                          <div class="text-center col-md-12">
                              <button type="submit" class="btn fw-bold w-100" style="background: #C9B09A; color: #fff">Masuk</button>
                          </div>
			      		</form>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		</div>
	</div>	

	<!-- JavaScript Bundle with Popper -->
	<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script> -->
</body>
</html>