<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PendudukController;
use App\Http\Controllers\DisposisiController;
use App\Http\Controllers\CetakSuratController;
use App\Http\Controllers\JenisSuratController;
use App\Http\Controllers\SuratMasukController;
use App\Http\Controllers\SuratKeluarController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes();

Route::group(['middleware' => 'auth'], function() {

    // Data Json 
Route::get('/penduduk/datajson', [PendudukController::class, 'dataJson'])->name('penduduk.json');
Route::get('/jenis_surat/datajson', [JenisSuratController::class, 'dataJson'])->name('jenis_surat.json');
Route::get('/surat_masuk/datajson', [SuratMasukController::class, 'dataJson'])->name('surat_masuk.json');
Route::get('/surat_keluar/datajson', [SuratKeluarController::class, 'dataJson'])->name('surat_keluar.json');
Route::get('/cetak/surat/datajson', [CetakSuratController::class, 'dataJson'])->name('cetak_surat.json');
Route::get('/disposisi/datajson', [DisposisiController::class, 'dataJson'])->name('disposisi.json');

Route::get('/dashboard', [HomeController::class, 'index'])->name('home');
Route::get('/surat_masuk', [CetakSuratController::class, 'index'])->name('surat');
Route::post('/surat_masuk', [CetakSuratController::class, 'store'])->name('surat.store');
Route::delete('/surat_masuk/{id}', [CetakSuratController::class, 'destroy'])->name('surat.destroy');
Route::get('/cetak_surat', [CetakSuratController::class, 'index'])->name('cetak_surat');
Route::get('/disposisi', [DisposisiController::class, 'index'])->name('disposisi');
Route::get('/disposisi/acc/{id}', [DisposisiController::class, 'acc_disposisi'])->name('acc.disposisi');
Route::get('/surat_keluar/acc/{id}', [SuratKeluarController::class, 'acc_surat_keluar'])->name('acc.surat_keluar');
Route::get('/surat_keluar/tolak/{id}', [SuratKeluarController::class, 'tolak_surat_keluar'])->name('tolak.surat_keluar');

    // PDF
Route::get('/foto_surat_masuk/pdf/{id}', [CetakSuratController::class, 'foto_surat_masuk'])->name('foto_surat_masuk');
Route::get('/skck/pdf/{id}', [CetakSuratController::class, 'pdf_skck'])->name('pdf_skck');
Route::get('/pindah_rumah/pdf/{id}', [CetakSuratController::class, 'pdf_pindah_rumah'])->name('pdf_pindah');
Route::get('/blm_pny_rmh/pdf/{id}', [CetakSuratController::class, 'pdf_blm_pny_rmh'])->name('pdf_blm_pny');
Route::get('/nikah_satu/pdf/{id}', [CetakSuratController::class, 'pdf_nikah_satu'])->name('pdf_nikah_satu');
Route::get('/nikah_empat/pdf/{id}', [CetakSuratController::class, 'pdf_nikah_empat'])->name('pdf_nikah_empat');
Route::get('/nikah_lima/pdf/{id}', [CetakSuratController::class, 'pdf_nikah_lima'])->name('pdf_nikah_lima');
Route::get('/disposisi/pdf/{id}', [CetakSuratController::class, 'cetak_disposisi'])->name('pdf_disposisi');

Route::get('/report/surat_masuk', [CetakSuratController::class, 'surat_masuk'])->name('report.surat_masuk');
Route::get('/report/surat_Keluar', [CetakSuratController::class, 'surat_keluar'])->name('report.surat_keluar');
Route::get('/report/disposisi', [CetakSuratController::class, 'disposisi'])->name('report.disposisi');
Route::get('/cetak_report/surat_masuk', [CetakSuratController::class, 'report_surat_masuk'])->name('cetak_report.surat_masuk');

Route::get('/report/pdf_surat_masuk', [CetakSuratController::class, 'pdf_surat_masuk'])->name('report.pdf_surat_masuk');
Route::get('/report/pdf_surat_keluar', [CetakSuratController::class, 'pdf_surat_keluar'])->name('report.pdf_surat_keluar');
Route::get('/report/pdf_disposisi', [CetakSuratController::class, 'pdf_disposisi'])->name('report.pdf_disposisi');

Route::resource('penduduk', PendudukController::class);
Route::resource('jenis_surat', JenisSuratController::class);
Route::resource('surat_masuk', SuratMasukController::class);
Route::resource('surat_keluar', SuratKeluarController::class);
Route::resource('manajemen_user', UserController::class);

// Manajemen User
// Route::get('/manajemen_user', [UserController::class, 'index'])->name('manajemen_user.index');
// Route::post('/manajemen_user', [UserController::class, 'store'])->name('manajemen_user.store');
// Route::get('/manajemen_user/{manajemen_user}/edit', [UserController::class, 'edit'])->name('edit');
// Route::patch('/manajemen_user/update/{manajemen_user}', [UserController::class, 'update'])->name('update');
// Route::get('/manajemen_user/show/{manajemen_user}', [UserController::class, 'show'])->name('show');

});