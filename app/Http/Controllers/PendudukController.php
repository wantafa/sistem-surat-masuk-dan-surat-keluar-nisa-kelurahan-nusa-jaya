<?php

namespace App\Http\Controllers;

use App\Models\Penduduk;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PendudukController extends Controller
{
    public function dataJson()
    {
        return DataTables::of(Penduduk::orderByDesc('id')->get())
            ->addColumn('action', function ($row) {

                $action = '<a href="javascript:void(0);" 
                data-nik="'.$row->nik.'" 
                data-nama="'.$row->nama.'" 
                data-tempat_lahir="'.$row->tempat_lahir.'"
                data-tgl_lahir="'.$row->tgl_lahir.'"
                data-jenis_kelamin="'.$row->jenis_kelamin.'"
                data-alamat="'.$row->alamat.'"
                data-agama="'.$row->agama.'"
                data-status_perkawinan="'.$row->status_perkawinan.'"
                data-pekerjaan="'.$row->pekerjaan.'"
                data-kewarganegaraan="'.$row->kewarganegaraan.'"
                class="btn btn-link btn-detail btn-info shadow btn-sm"> 
                <i class="fa fa-eye"></i></a>

                
                <a href="javascript:void(0);" class="btn btn-link btn-primary btn-md btn-edit" data-id="'.$row->id.'" data-nik="'.$row->nik.'" data-nama="'.$row->nama.'" data-tempat_lahir="'.$row->tempat_lahir.'" data-tgl_lahir="'.$row->tgl_lahir.'" data-jenis_kelamin="'.$row->jenis_kelamin.'" data-alamat="'.$row->alamat.'" data-agama="'.$row->agama.'" data-status_perkawinan="'.$row->status_perkawinan.'" data-pekerjaan="'.$row->pekerjaan.'" data-kewarganegaraan="'.$row->kewarganegaraan.'"><i class="fa fa-edit"></i></a> 
                
                <a href="javascript:void(0);" data-id="'.$row->id.'" class="btn btn-link btn-danger btn-md btn-delete"><i class="fa fa-times"></i></a>';
                return $action;
            })
            ->make(true);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 'sekretaris' || Auth::user()->role == 'lurah') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        return view('penduduk.index');
    }

    
     public function store(Request $request)
    {
        $request->validate([
            'nik' => 'required',
            'nama' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'agama' => 'required',
            'status_perkawinan' => 'required',
            'pekerjaan' => 'required',
            'kewarganegaraan' => 'required',
        ]);

        $req_penduduk = $request->all();

        if ($request->id) {

            $penduduk = Penduduk::find($request->id);
            $penduduk->update($req_penduduk);
            $message = "Data Penduduk Berhasil diupdate";


        } else {

            $penduduk = Penduduk::create($req_penduduk);
            $message = "Data Penduduk Berhasil Disimpan";

        }

        return back()->with('success', $message);
    }


    public function destroy($id)
    {
        $penduduk = Penduduk::find($id);
        $penduduk->delete();

        return back();
    }
}
