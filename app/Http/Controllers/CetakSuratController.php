<?php

namespace App\Http\Controllers;

use App\Models\Skck;
use App\Models\Nlima;
use App\Models\Nsatu;
use App\Models\Nempat;
use App\Models\Penduduk;
use App\Models\SuratMasuk;
use App\Models\PindahRumah;
use App\Models\SuratKeluar;
use Illuminate\Http\Request;
use App\Models\BelomPunyaRumah;
use App\Models\Disposisi;
use App\Models\JenisSurat;
use Barryvdh\DomPDF\Facade\Pdf;
use Yajra\DataTables\DataTables;

class CetakSuratController extends Controller
{
    public function dataJson()
    {
        $penduduk = SuratKeluar::join('penduduk', 'penduduk.id', 'surat_keluar.penduduk_id')->join('jenis_surat', 'jenis_surat.id', 'surat_keluar.jenis_surat_id')->select('penduduk.id as id_penduduk', 'penduduk.nama', 'surat_keluar.no_surat', 'jenis_surat.nama_surat')->orderByDesc('penduduk.id')->get();

        return DataTables::of($penduduk)
            ->addColumn('skck', function ($row) {

                if ($row->nama_surat == 'SURAT PENGANTAR CATATAN KEPOLISIAN') {
                    $skck = '<a href="'.route('pdf_skck', $row->id_penduduk).'" class="btn btn-success btn-sm"><i class="fa fa-file-pdf"></i></a>';
                    return $skck;
                }
            })
            ->addColumn('pindah_rumah', function ($row) {

                if ($row->nama_surat == 'SURAT PENGANTAR PINDAH ANTAR KOTA/KABUPATEN/PROVINSI') {
                    $pindah_rumah = '<a href="'.route('pdf_pindah', $row->id_penduduk).'" class="btn btn-success btn-sm"><i class="fa fa-file-pdf"></i></a>';
                    return $pindah_rumah;
                }
            })
            ->addColumn('blm_pny_rmh', function ($row) {

                if ($row->nama_surat == 'SURAT KETERANGAN BELUM MEMILIKI RUMAH') {
                    $blm_pny_rmh = '<a href="'.route('pdf_blm_pny', $row->id_penduduk).'" class="btn btn-success btn-sm"><i class="fa fa-file-pdf"></i></a>';
                    return $blm_pny_rmh;
                }
            })
            ->addColumn('nikah', function ($row) {

                if ($row->nama_surat == 'SURAT NIKAH') {
                    $nikah = '<a href="'.route('pdf_nikah_satu', $row->id_penduduk).'" class="btn btn-success btn-sm">N1</i></a>
                            <a href="'.route('pdf_nikah_empat', $row->id_penduduk).'" class="btn btn-warning btn-sm">N4</i></a>
                            <a href="'.route('pdf_nikah_lima', $row->id_penduduk).'" class="btn btn-info btn-sm">N5</i></a>';
                    return $nikah;
                }
            })
            ->addIndexColumn()
            ->rawColumns(['skck', 'pindah_rumah', 'blm_pny_rmh', 'nikah'])
            ->make(true);

    }

    public function index()
    {
        
        return view('cetak_surat.index');

    }

    public function foto_surat_masuk($id)
    {
        $foto = SuratMasuk::where('id', '=', $id)->select('surat_masuk.image','surat_masuk.no_surat')->first();

        $pdf = Pdf::loadView('cetak_surat.pdf.foto_surat_masuk', compact('foto'))->setPaper('legal')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('uploads/image')]);
        // dd($foto->image);
        return $pdf->stream('Suurat Masuk - '.$foto->no_surat.'.pdf');
    }
    public function pdf_skck($id)
    {
        $skck = Skck::join('surat_keluar', 'surat_keluar.id', '=', 'sk_skck.surat_keluar_id')
                    ->join('penduduk', 'penduduk.id', 'surat_keluar.penduduk_id')
                    ->where('penduduk.id', '=', $id)
                    ->select('sk_skck.*', 'penduduk.nama', 'penduduk.jenis_kelamin', 'penduduk.tempat_lahir', 'penduduk.tgl_lahir', 'penduduk.agama', 'penduduk.nik', 'penduduk.alamat', 'surat_keluar.tanggal', 'surat_keluar.no_surat')->first();

        $pdf = Pdf::loadView('cetak_surat.pdf.skck_pdf', compact('skck'))->setPaper('legal')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('assets/img')]);
        
        return $pdf->download('SKCK - '.$skck->nama.'.pdf');
    }
    
    public function pdf_pindah_rumah($id)
    {
        $pindah_rumah = PindahRumah::join('surat_keluar', 'surat_keluar.id', '=', 'sk_pindah_rumah.surat_keluar_id')
                    ->join('penduduk', 'penduduk.id', 'surat_keluar.penduduk_id')
                    ->where('penduduk.id', '=', $id)
                    ->select('sk_pindah_rumah.*', 'penduduk.nama', 'penduduk.nik', 'penduduk.jenis_kelamin', 'penduduk.tempat_lahir', 'penduduk.tgl_lahir', 'penduduk.nik', 'penduduk.alamat', 'penduduk.pekerjaan', 'penduduk.status_perkawinan', 'surat_keluar.tanggal', 'surat_keluar.no_surat')->first();

        $pdf = Pdf::loadView('cetak_surat.pdf.pindah_pdf', compact('pindah_rumah'))->setPaper('legal')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('assets/img')]);

        return $pdf->download('Surat Pindah Rumah - '.$pindah_rumah->nama.'.pdf');
    }
    
    public function pdf_blm_pny_rmh($id)
    {
        $blm_pny_rmh = BelomPunyaRumah::join('surat_keluar', 'surat_keluar.id', '=', 'sk_belum_punya_rumah.surat_keluar_id')
                    ->join('penduduk', 'penduduk.id', 'surat_keluar.penduduk_id')
                    ->where('penduduk.id', '=', $id)
                    ->select('sk_belum_punya_rumah.*', 'penduduk.nama', 'penduduk.jenis_kelamin', 'penduduk.tempat_lahir', 'penduduk.kewarganegaraan', 'penduduk.agama', 'penduduk.pekerjaan', 'penduduk.tgl_lahir', 'penduduk.nik', 'penduduk.alamat', 'surat_keluar.tanggal', 'penduduk.nik', 'surat_keluar.no_surat')->first();

        $pdf = Pdf::loadView('cetak_surat.pdf.blm_pny_rmh_pdf', compact('blm_pny_rmh'))->setPaper('legal')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('assets/img')]);

        return $pdf->download('Surat Belum Punya Rumah - '.$blm_pny_rmh->nama.'.pdf');
    }
    
    public function pdf_nikah_satu($id)
    {
        $nikah = Nsatu::join('sk_surat_nikah', 'sk_surat_nikah.id', '=', 'sk_nikah_n1.surat_nikah_id')
                    ->join('surat_keluar', 'surat_keluar.id', 'sk_surat_nikah.surat_keluar_id')
                    ->join('penduduk', 'penduduk.id', 'surat_keluar.penduduk_id')
                    ->where('penduduk.id', '=', $id)
                    ->select('sk_nikah_n1.*', 'surat_keluar.tanggal', 'surat_keluar.no_surat')->first();

        $calon = Penduduk::where('id', $nikah->penduduk_id_calon)->first();
        $ayah = Penduduk::where('id', $nikah->penduduk_id_ayah)->first();
        $ibu = Penduduk::where('id', $nikah->penduduk_id_ibu)->first();

        $pdf = Pdf::loadView('cetak_surat.pdf.nikah_satu_pdf', compact('calon', 'ayah', 'ibu', 'nikah'))->setPaper('legal')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('assets/img')]);

        return $pdf->stream('Surat Nikah N1 - '.$nikah->nama.'.pdf');
    }
    
    public function pdf_nikah_empat($id)
    {
        $nikah = Nempat::join('sk_surat_nikah', 'sk_surat_nikah.id', '=', 'sk_nikah_n4.surat_nikah_id')
                    ->join('surat_keluar', 'surat_keluar.id', 'sk_surat_nikah.surat_keluar_id')
                    ->join('penduduk', 'penduduk.id', 'surat_keluar.penduduk_id')
                    ->where('penduduk.id', '=', $id)
                    ->select('sk_nikah_n4.*', 'surat_keluar.tanggal', 'surat_keluar.no_surat')->first();

        $calon_suami = Penduduk::where('id', $nikah->penduduk_id_calon_suami)->first();
        $calon_istri = Penduduk::where('id', $nikah->penduduk_id_calon_istri)->first();

        $pdf = Pdf::loadView('cetak_surat.pdf.nikah_empat_pdf', compact('nikah', 'calon_suami', 'calon_istri'))->setPaper('legal')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('assets/img')]);

        return $pdf->stream('Surat Nikah N4 - '.$nikah->nama.'.pdf');
    }

    public function pdf_nikah_lima($id)
    {
        $nikah = Nlima::join('sk_surat_nikah', 'sk_surat_nikah.id', '=', 'sk_nikah_n5.surat_nikah_id')
                    ->join('surat_keluar', 'surat_keluar.id', 'sk_surat_nikah.surat_keluar_id')
                    ->join('penduduk', 'penduduk.id', 'surat_keluar.penduduk_id')
                    ->where('penduduk.id', '=', $id)
                    ->select('sk_nikah_n5.*', 'surat_keluar.tanggal', 'surat_keluar.no_surat')->first();

        $calon_suami = Penduduk::where('id', $nikah->penduduk_id_calon_suami)->first();
        $calon_istri = Penduduk::where('id', $nikah->penduduk_id_calon_istri)->first();
        $ayah_calon = Penduduk::where('id', $nikah->penduduk_id_ayah_calon)->first();
        $ibu_calon = Penduduk::where('id', $nikah->penduduk_id_ibu_calon)->first();

        $pdf = Pdf::loadView('cetak_surat.pdf.nikah_lima_pdf', compact('nikah', 'calon_suami', 'calon_istri', 'ayah_calon', 'ibu_calon'))->setPaper('legal')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('assets/img')]);

        return $pdf->stream('Surat Nikah N5 - '.$nikah->nama.'.pdf');
    }

    // REPORT LAPORAN
    
    public function surat_masuk(Request $request)
    {
        $no = 1;
        
        $get_jenis = JenisSurat::where('jenis_surat', 'Surat Masuk')->get();
        
        $tgl_masuk = $request->tgl_masuk;
        $jenis_surat = $request->jenis_surat;
        $perihal = $request->perihal;

        $cari = SuratMasuk::where('tanggal', $tgl_masuk)->orWhere('jenis_surat_id', $jenis_surat)->orWhere('perihal', $perihal)->latest()->get();
        
        return view('cetak_surat.surat_masuk', compact('cari','no', 'tgl_masuk', 'perihal', 'jenis_surat', 'get_jenis'));

    }
    
    public function surat_keluar(Request $request)
    {
        $no = 1;

        $get_jenis = JenisSurat::where('jenis_surat', 'Surat Keluar')->get();

        $tgl_keluar = $request->tgl_keluar;
        $jenis_surat = $request->jenis_surat;
        $perihal = $request->perihal;

        $cari = SuratKeluar::where('tanggal', $tgl_keluar)->orWhere('jenis_surat_id', $jenis_surat)->orWhere('perihal', $perihal)->latest()->get();
		
        return view('cetak_surat.surat_keluar', compact('cari','no', 'tgl_keluar', 'perihal', 'jenis_surat', 'get_jenis'));   

    }
    
    public function disposisi(Request $request)
    {
        $no = 1;

        $tgl_disposisi = $request->tgl_disposisi;
        $asal = $request->asal;
        $perihal = $request->perihal;

        $cari = Disposisi::join('surat_masuk', 'surat_masuk.disposisi_id', 'disposisi.id')
        ->where('tanggal', $tgl_disposisi)->orWhere('asal', $asal)->orWhere('perihal', $perihal)->latest()
        ->select('disposisi.*')
        ->get();
		
        
        return view('cetak_surat.disposisi', compact('cari','no', 'tgl_disposisi', 'asal', 'perihal'));

    }

    public function pdf_surat_masuk(Request $request)
    {
        $no = 1;

        $tgl_masuk = $request->get('tgl_masuk');

        $surat_masuk = SuratMasuk::where('tanggal','like',"%".$tgl_masuk."%")->latest()->get();

        $pdf = Pdf::loadView('cetak_surat.pdf_report.report_surat_masuk', compact('surat_masuk'))->setPaper('legal')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('/img')]);

        return $pdf->download('Laporan Surat Masuk.pdf');
		
    }
    
    public function pdf_surat_keluar(Request $request)
    {
        $no = 1;

        $tgl_keluar = $request->get('tgl_keluar');

        $surat_keluar = SuratKeluar::where('tanggal','like',"%".$tgl_keluar."%")->latest()->get();

        $pdf = Pdf::loadView('cetak_surat.pdf_report.report_surat_keluar', compact('surat_keluar'))->setPaper('legal')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('/img')]);

        return $pdf->download('Laporan Surat Keluar.pdf');
		
    }

    public function pdf_disposisi(Request $request)
    {
        $no = 1;

        $tgl_disposisi = $request->tgl_disposisi;

        $disposisi = Disposisi::join('surat_masuk', 'surat_masuk.disposisi_id', 'disposisi.id')
                    ->join('jenis_surat', 'jenis_surat.id', 'surat_masuk.jenis_surat_id')
                    ->where('surat_masuk.tanggal','like',"%".$tgl_disposisi."%")->latest()
                    ->select('disposisi.*', 'surat_masuk.no_surat', 'surat_masuk.perihal', 'jenis_surat.nama_surat')
                    ->get();

        $pdf = Pdf::loadView('cetak_surat.pdf_report.report_disposisi', compact('disposisi'))->setPaper('legal')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('/img')]);

        return $pdf->download('Laporan Disposisi.pdf');
		
    }

    public function cetak_disposisi($id)
    {
        $disposisi = Disposisi::join('surat_masuk', 'surat_masuk.disposisi_id', '=', 'disposisi.id')
                    ->where('disposisi.id', '=', $id)
                    ->select('disposisi.*', 'surat_masuk.perihal', 'surat_masuk.tanggal', 'surat_masuk.no_surat')->first();
                    
        $pdf = Pdf::loadView('disposisi.disposisi_pdf', compact('disposisi'))->setPaper('a4')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('assets/img')]);
        
        return $pdf->stream('Disposisi.pdf');
    }
}
