<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\SuratMasuk;
use App\Models\SuratKeluar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        toast('Login Berhasil', 'success')->position('center')->width('300px')->timerProgressBar()->background('#ffffff');

        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // total Surat Masuk
        $lurah = SuratMasuk::where('jenis_surat_id', 6)->count(); 
        $sekretaris = SuratMasuk::where('jenis_surat_id', 7)->count(); 
        $tata_pemerintahan = SuratMasuk::where('jenis_surat_id', 5)->count(); 
        $ekonomi = SuratMasuk::where('jenis_surat_id', 8)->count(); 
        $pemberdayaan = SuratMasuk::where('jenis_surat_id', 9)->count(); 

        // total Surat Keluar
        $skck = SuratKeluar::where('jenis_surat_id', 1)->count(); 
        $pindah = SuratKeluar::where('jenis_surat_id', 2)->count(); 
        $blm_pny_rmh = SuratKeluar::where('jenis_surat_id', 3)->count(); 
        $nikah = SuratKeluar::where('jenis_surat_id', 4)->count();


        $surat_masuk= DB::table('surat_masuk')
                    ->join('jenis_surat', 'jenis_surat.id', 'surat_masuk.jenis_surat_id')
                    ->whereMonth('tanggal', Carbon::now()->format('m'))
                    ->select(DB::raw('count(jenis_surat.nama_surat) as total, jenis_surat.nama_surat'))
                    ->groupBy('jenis_surat.nama_surat')
                    ->get();
        
        $surat_keluar= DB::table('surat_keluar')
                    ->join('jenis_surat', 'jenis_surat.id', 'surat_keluar.jenis_surat_id')
                    ->whereMonth('tanggal', Carbon::now()->format('m'))
                    ->select(DB::raw('count(jenis_surat.nama_surat) as total, jenis_surat.nama_surat'))
                    ->groupBy('jenis_surat.nama_surat')
                    ->get();
        $no = 1;


        return view('dashboard', compact('lurah', 'sekretaris', 'tata_pemerintahan', 'ekonomi', 'pemberdayaan', 'skck', 'pindah', 'blm_pny_rmh', 'nikah', 'surat_masuk', 'surat_keluar'));
    }
}
