<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Skck;
use App\Models\Penduduk;
use App\Models\JenisSurat;
use App\Models\SuratNikah;
use App\Models\PindahRumah;
use App\Models\SuratKeluar;
use Illuminate\Http\Request;
use App\Models\BelomPunyaRumah;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class SuratKeluarController extends Controller
{
    public function dataJson()
    // with('jenis_surat','skck', 'pindah_rumah', 'blm_pny_rmh', 'surat_nikah','nsatu','nempat','nlima')
    {
        $surat_keluar = SuratKeluar::
                        // leftjoin('jenis_surat', 'jenis_surat.id', '=', 'surat_keluar.jenis_surat_id')
                        // ->leftjoin('penduduk', 'penduduk.id', '=', 'sk_surat_nikah.penduduk_id')
                        leftjoin('sk_skck', 'surat_keluar.id', '=', 'sk_skck.surat_keluar_id')
                        ->leftjoin('sk_pindah_rumah', 'surat_keluar.id', '=', 'sk_pindah_rumah.surat_keluar_id')
                        ->leftjoin('sk_belum_punya_rumah', 'surat_keluar.id', '=', 'sk_belum_punya_rumah.surat_keluar_id',)
                        ->leftjoin('sk_surat_nikah', 'surat_keluar.id', '=', 'sk_surat_nikah.surat_keluar_id')
                        ->leftjoin('sk_nikah_n1', 'sk_surat_nikah.id', '=', 'sk_nikah_n1.surat_nikah_id')
                        ->leftjoin('sk_nikah_n4', 'sk_surat_nikah.id', '=', 'sk_nikah_n4.surat_nikah_id')
                        ->leftjoin('sk_nikah_n5', 'sk_surat_nikah.id', '=', 'sk_nikah_n5.surat_nikah_id')
                        ->select('surat_keluar.*', 'sk_skck.keterangan as keterangan_skck', 
                        
                        'sk_pindah_rumah.keterangan as keterangan_pindah',
                        'sk_pindah_rumah.no_kartu_keluarga',
                        'sk_pindah_rumah.alamat_pindah',
                        'sk_pindah_rumah.jumlah_pengikut',
                        
                        'sk_nikah_n1.penduduk_id_calon',
                        'sk_nikah_n1.penduduk_id_ayah',
                        'sk_nikah_n1.penduduk_id_ibu',
                        
                        'sk_nikah_n4.penduduk_id_calon_suami',
                        'sk_nikah_n4.penduduk_id_calon_istri',
                        
                        'sk_nikah_n5.penduduk_id_calon_suami',
                        'sk_nikah_n5.penduduk_id_calon_istri',
                        'sk_nikah_n5.penduduk_id_ayah_calon',
                        'sk_nikah_n5.ortu_ayah',
                        'sk_nikah_n5.penduduk_id_ibu_calon',
                        'sk_nikah_n5.ortu_ibu',
                        'sk_nikah_n5.ortu_calon',
                        
                        )
                        ->get();
        // dd($surat_keluar);
        return DataTables::of($surat_keluar)
            ->addColumn('action', function ($row) {

                $action = '<a href="javascript:void(0);" 
                data-tanggal="'.$row->tanggal.'" 
                data-no_surat="'.$row->no_surat.'"
                data-jenis_surat="'.$row->jenis_surat->nama_surat.'"
                data-lampiran="'.$row->lampiran.'"
                data-perihal="'.$row->perihal.'"
                data-verification="'.$row->verification.'"
                data-nama_lurah="'.$row->nama_lurah.'"
                data-no_kartu_keluarga="'.$row->no_kartu_keluarga.'"
                class="btn btn-link btn-detail btn-info btn-sm"> 
                <i class="fa fa-eye"></i></a>
                
                <a href="javascript:void(0);" class="btn btn-link btn-primary btn-sm btn-edit" data-id="'.$row->id.'" data-tanggal="'.$row->tanggal.'" 
                data-no_surat="'.$row->no_surat.'"
                data-penduduk_id="'.$row->penduduk_id.'" 
                data-jenis_surat_id="'.$row->jenis_surat_id.'"
                data-jenis_surat="'.$row->jenis_surat->nama_surat.'"
                data-lampiran="'.$row->lampiran.'"
                data-perihal="'.$row->perihal.'"
                data-nama_lurah="'.$row->nama_lurah.'"
                data-verification="'.$row->verification.'"
                data-keterangan_skck="'.$row->keterangan_skck.'"

                data-keterangan_pindah="'.$row->keterangan_pindah.'"
                data-no_kartu_keluarga="'.$row->no_kartu_keluarga.'"
                data-alamat_pindah="'.$row->alamat_pindah.'"
                data-jumlah_pengikut="'.$row->jumlah_pengikut.'"
                
                data-penduduk_id_calon="'.$row->penduduk_id_calon.'"
                data-penduduk_id_ayah="'.$row->penduduk_id_ayah.'"
                data-penduduk_id_ibu="'.$row->penduduk_id_ibu.'"

                data-penduduk_id_calon_suami="'.$row->penduduk_id_calon_suami.'"
                data-penduduk_id_calon_istri="'.$row->penduduk_id_calon_istri.'"
                data-penduduk_id_ayah_calon="'.$row->penduduk_id_ayah_calon.'"
                data-ortu_ayah="'.$row->ortu_ayah.'"
                data-penduduk_id_ibu_calon="'.$row->penduduk_id_ibu_calon.'"
                data-ortu_ibu="'.$row->ortu_ibu.'"
                data-ortu_calon="'.$row->ortu_calon.'"><i class="fa fa-edit"></i></a> 
                
                <a href="javascript:void(0);" data-id="'.$row->id.'" class="btn btn-link btn-danger btn-sm btn-delete"><i class="fa fa-times"></i></a>';
                return $action;
            })
            ->editColumn('jenis_surat', function ($row) {
                return $row->jenis_surat->nama_surat;
            })
            ->editColumn('tanggal', function ($row) {
                return Carbon::create($row->tanggal)->translatedFormat('d F Y');
            })
            ->addColumn('status', function ($row) {
                if ($row->status == "Pending") {
                    $status = $row->status .'<br>'. '<a href="'.route('acc.surat_keluar', $row->id).'" data-id="'.$row->id.'" class="btn btn-success btn-acc btn-sm"><i class="fa fa-check"></i></a>
                    
                    <a href="'.route('tolak.surat_keluar', $row->id).'" data-id="'.$row->id.'" class="btn btn-danger btn-tolak btn-sm"><i class="fa fa-times"></i></a>';
                } elseif($row->status == "Ditolak") {
                    $status = $row->status .'<br>'. '<a href="'.route('acc.surat_keluar', $row->id).'" data-id="'.$row->id.'" class="btn btn-success btn-acc btn-sm"><i class="fa fa-check"></i></a>';
                } else {
                    $status = $row->status;
                }
                return $status;
            })
            ->rawColumns(['status','action'])
            ->make(true);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 'lurah') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }   
        $jenis_surat = JenisSurat::where('jenis_surat', 'Surat Keluar')->get();
        $penduduk = Penduduk::all();
        return view('surat_keluar.index', compact('jenis_surat', 'penduduk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jenis_surat_id' => 'required',
            'tanggal' => 'required',
            'no_surat' => 'required',
            'lampiran' => 'required',
            'perihal' => 'required',
            'verification' => 'required',
            'nama_lurah' => 'required',
        ]);

        $req_surat_keluar = $request->all();
        $req_surat_keluar['created_by'] = Auth::user()->id;
        if ($request->id) {

            $surat_keluar = SuratKeluar::find($request->id);

            $req_surat_keluar['updated_by'] = Auth::user()->id;
            
            $surat_keluar->update($req_surat_keluar);

            unset($req_surat_keluar['_token']);
            if ($request->jenis == 'skck') {

                Skck::where('surat_keluar_id', $request->id)->update([
                    'keterangan' => $req_surat_keluar['keterangan_skck'], 
                    'created_by' => $req_surat_keluar['created_by'],
                ]);

            } elseif ($request->jenis == 'pindah') {

                PindahRumah::where('surat_keluar_id', $request->id)->update([
                    'keterangan' => $req_surat_keluar['keterangan_pindah'], 
                    'no_kartu_keluarga' => $req_surat_keluar['no_kartu_keluarga'], 
                    'alamat_pindah' => $req_surat_keluar['alamat_pindah'], 
                    'jumlah_pengikut' => $req_surat_keluar['jumlah_pengikut'], 
                    'created_by' => $req_surat_keluar['created_by'],

                ]);

            } elseif ($request->jenis == 'nikah') {
                
                $surat_nikah = SuratNikah::where('surat_keluar_id', $request->id)->update($req_surat_keluar);

                $req_surat_keluar['surat_nikah_id'] = $surat_nikah->id;
                
                DB::table('sk_nikah_n1')->where('surat_nikah_id', $request->surat_nikah_id)->update([
                    'penduduk_id_calon' => $req_surat_keluar['penduduk_id_calon'],
                    'penduduk_id_ayah' => $req_surat_keluar['penduduk_id_ayah'],
                    'penduduk_id_ibu' => $req_surat_keluar['penduduk_id_ibu'],
                    'created_by' => $req_surat_keluar['created_by'],
                ]);
                DB::table('sk_nikah_n4')->where('surat_nikah_id', $request->surat_nikah_id)->update([
                    'penduduk_id_calon_suami' => $req_surat_keluar['penduduk_id_calon_suami'],
                    'penduduk_id_calon_istri' => $req_surat_keluar['penduduk_id_calon_istri'],
                    'created_by' => $req_surat_keluar['created_by'],
                ]);
                DB::table('sk_nikah_n5')->where('surat_nikah_id', $request->surat_nikah_id)->update([
                    'penduduk_id_calon_suami' => $req_surat_keluar['penduduk_id_calon_suami'],
                    'penduduk_id_calon_istri' => $req_surat_keluar['penduduk_id_calon_istri'],
                    'penduduk_id_ayah_calon' => $req_surat_keluar['penduduk_id_ayah_calon'],
                    'ortu_ayah' => $req_surat_keluar['ortu_ayah'],
                    'penduduk_id_ibu_calon' => $req_surat_keluar['penduduk_id_ibu_calon'],
                    'ortu_ibu' => $req_surat_keluar['ortu_ibu'],
                    'ortu_calon' => $req_surat_keluar['ortu_calon'],
                    'created_by' => $req_surat_keluar['created_by'],
                ]);
            }

            $message = "Data Surat Keluar Berhasil diupdate";


        } else {

            $req_surat_keluar['status'] = 'Pending';

            $surat_keluar = SuratKeluar::create($req_surat_keluar);

            $req_surat_keluar['surat_keluar_id'] = $surat_keluar->id;
            
            if ($request->jenis == 'skck') {

                DB::table('sk_skck')->insert([
                    'surat_keluar_id' => $req_surat_keluar['surat_keluar_id'], 
                    'keterangan' => $req_surat_keluar['keterangan_skck'], 
                    'created_by' => $req_surat_keluar['created_by'], 
                ]);

            } elseif ($request->jenis == 'pindah') {

                DB::table('sk_pindah_rumah')->insert([
                    'surat_keluar_id' => $req_surat_keluar['surat_keluar_id'], 
                    'keterangan' => $req_surat_keluar['keterangan_pindah'], 
                    'no_kartu_keluarga' => $req_surat_keluar['no_kartu_keluarga'], 
                    'alamat_pindah' => $req_surat_keluar['alamat_pindah'], 
                    'jumlah_pengikut' => $req_surat_keluar['jumlah_pengikut'], 
                    'created_by' => $req_surat_keluar['created_by'], 
                ]);

            } elseif ($request->jenis == 'blm_pny_rmh') {

                DB::table('sk_belum_punya_rumah')->insert([
                    'surat_keluar_id' => $req_surat_keluar['surat_keluar_id'], 
                    'created_by' => $req_surat_keluar['created_by'], 
                ]);

            } elseif ($request->jenis == 'nikah') {
                
                $surat_nikah = SuratNikah::create([
                    'surat_keluar_id' => $req_surat_keluar['surat_keluar_id'], 
                    'created_by' => $req_surat_keluar['created_by'], 
                ]);

                $req_surat_keluar['surat_nikah_id'] = $surat_nikah->id;
                
                DB::table('sk_nikah_n1')->insert([
                    'surat_nikah_id' => $req_surat_keluar['surat_nikah_id'],
                    'penduduk_id_calon' => $req_surat_keluar['penduduk_id_calon'],
                    'penduduk_id_ayah' => $req_surat_keluar['penduduk_id_ayah'],
                    'penduduk_id_ibu' => $req_surat_keluar['penduduk_id_ibu'],
                    'created_by' => $req_surat_keluar['created_by'],
                ]);
                DB::table('sk_nikah_n4')->insert([
                    'surat_nikah_id' => $req_surat_keluar['surat_nikah_id'],
                    'penduduk_id_calon_suami' => $req_surat_keluar['penduduk_id_calon_suami'],
                    'penduduk_id_calon_istri' => $req_surat_keluar['penduduk_id_calon_istri'],
                    'created_by' => $req_surat_keluar['created_by'],
                ]);
                DB::table('sk_nikah_n5')->insert([
                    'surat_nikah_id' => $req_surat_keluar['surat_nikah_id'],
                    'penduduk_id_calon_suami' => $req_surat_keluar['penduduk_id_calon_suami'],
                    'penduduk_id_calon_istri' => $req_surat_keluar['penduduk_id_calon_istri'],
                    'penduduk_id_ayah_calon' => $req_surat_keluar['penduduk_id_ayah_calon'],
                    'ortu_ayah' => $req_surat_keluar['ortu_ayah'],
                    'penduduk_id_ibu_calon' => $req_surat_keluar['penduduk_id_ibu_calon'],
                    'ortu_ibu' => $req_surat_keluar['ortu_ibu'],
                    'ortu_calon' => $req_surat_keluar['ortu_calon'],
                    'created_by' => $req_surat_keluar['created_by'],
                ]);
            }
            
            $message = "Data Surat Keluar Berhasil Disimpan";

        }

        return back()->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SuratKeluar  $suratKeluar
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $surat_keluar= SuratKeluar::find($id);
        $surat_keluar->delete();

        return back();
    }

    public function acc_surat_keluar($id)
    {
        SuratKeluar::where('id', $id)->update([
            'status' => 'Terima'
        ]);

        return back();
    }
    
    public function tolak_surat_keluar($id)
    {
        SuratKeluar::where('id', $id)->update([
            'status' => 'Ditolak'
        ]);

        return back();
    }
}
