<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Disposisi;
use App\Models\JenisSurat;
use App\Models\SuratMasuk;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class SuratMasukController extends Controller
{
    public function dataJson()
    {
        return DataTables::of(SuratMasuk::with('jenis_surat','disposisi')->orderByDesc('id')->get())
            ->addColumn('action', function ($row) {

                $action = '<a href="javascript:void(0);" 
                data-tanggal="'.$row->tanggal.'" 
                data-no_surat="'.$row->no_surat.'"
                data-jenis_surat="'.$row->jenis_surat->nama_surat.'"
                data-image="'.$row->image.'"
                data-perihal="'.$row->perihal.'"
                data-asal="'.$row->disposisi->asal.'"
                data-intruksi="'.$row->disposisi->intruksi.'"
                data-diteruskan="'.$row->disposisi->diteruskan.'"
                data-tgl_penyelesaian="'.$row->disposisi->tgl_penyelesaian.'"
                class="btn btn-link btn-detail btn-info shadow btn-sm"> 
                <i class="fa fa-eye"></i></a>

                
                <a href="javascript:void(0);" class="btn btn-link btn-primary btn-md btn-edit" data-id="'.$row->id.'" data-tanggal="'.$row->tanggal.'" data-no_surat="'.$row->no_surat.'" data-jenis_surat_id="'.$row->jenis_surat_id.'" data-perihal="'.$row->perihal.'" data-asal="'.$row->disposisi->asal.'" data-intruksi="'.$row->disposisi->intruksi.'" data-diteruskan="'.$row->disposisi->diteruskan.'" data-tgl_penyelesaian="'.$row->disposisi->tgl_penyelesaian.'"><i class="fa fa-edit"></i></a> 
                
                <a href="javascript:void(0);" data-id="'.$row->id.'" class="btn btn-link btn-danger btn-md btn-delete"><i class="fa fa-times"></i></a>';
                return $action;
            })
            ->editColumn('jenis_surat', function ($row) {
                return $row->jenis_surat->nama_surat;
            })
            ->editColumn('image', function ($row) {
                $skck = '<a href="'.route('foto_surat_masuk', $row->id).'" class="btn btn-primary btn-sm"><i class="fa fa-image"></i></a>';
                return $skck;
            })
            ->editColumn('tanggal', function ($row) {
                return Carbon::create($row->tanggal)->translatedFormat('d F Y');
            })
            ->rawColumns(['jenis_surat', 'image', 'action', 'tanggal'])
            ->make(true);

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 'sekretaris' || Auth::user()->role == 'lurah') {
            Alert::error('Maaf..', 'Anda dilarang masuk ke area ini.');
            return redirect()->to('/dashboard');
        }
        $jenis_surat = JenisSurat::where('jenis_surat', 'Surat Masuk')->get();
        return view('surat_masuk.index', compact('jenis_surat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tanggal' => 'required',
            'no_surat' => 'required',
            'perihal' => 'required',
        ]);

        $req_surat_masuk = $request->all();

        if ($request->id) {

            $surat_masuk = SuratMasuk::find($request->id);

            if ($request->hasFile('image')) {
                File::delete($surat_masuk->image);
    
                $image = 'uploads/image/'. time() . '_' . $request->file('image')->getClientOriginalName();
                $request->file('image')->move('uploads/image', $image);
                $req_surat_masuk['image'] = $image;

            }

            
            $disposisi = Disposisi::where('id', $surat_masuk->disposisi_id)->first();

            $req_surat_masuk['disposisi_id'] = $disposisi->id;
            $req_surat_masuk['updated_by'] = Auth::user()->id;
            
            $surat_masuk->update($req_surat_masuk);
            $disposisi->update($req_surat_masuk);

            $message = "Data Surat Masuk Berhasil diupdate";


        } else {

            if ($request->hasFile('image')) {
                $image = 'uploads/image/'. time() . '.' . $request->file('image')->getClientOriginalName(); 
                $request->file('image')->move('uploads/image', $image);
                
                $req_surat_masuk['image'] = $image;
            }
    
            $req_surat_masuk['created_by'] = Auth::user()->id;
            $req_surat_masuk['status'] = 'Pending';

            $disposisi = Disposisi::create($req_surat_masuk);
            $req_surat_masuk['disposisi_id'] = $disposisi->id;

            $surat_masuk = SuratMasuk::create($req_surat_masuk);


            $message = "Data Surat Masuk Berhasil Disimpan";

        }

        return back()->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SuratMasuk  $suratMasuk
     * @return \Illuminate\Http\Response
     */
    public function show(SuratMasuk $suratMasuk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SuratMasuk  $suratMasuk
     * @return \Illuminate\Http\Response
     */
    public function edit(SuratMasuk $suratMasuk)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SuratMasuk  $suratMasuk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SuratMasuk $suratMasuk)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SuratMasuk  $suratMasuk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $surat_masuk = SuratMasuk::find($id);
        $surat_masuk->delete();

        return back();
    }
}
