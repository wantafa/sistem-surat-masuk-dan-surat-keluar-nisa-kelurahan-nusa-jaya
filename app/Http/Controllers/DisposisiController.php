<?php

namespace App\Http\Controllers;

use App\Models\Disposisi;
use Carbon\Carbon;
use App\Models\SuratMasuk;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DisposisiController extends Controller
{
    public function dataJson()
    {
        return DataTables::of(Disposisi::join('surat_masuk', 'surat_masuk.disposisi_id', 'disposisi.id')->where('status', 'Pending')->orderByDesc('surat_masuk.id')->get())
            ->addColumn('action', function ($row) {

                $action = '<a href="javascript:void(0);" data-id="'.$row->id.'" class="btn btn-success btn-acc shadow btn-sm">Proses</a>
                
                <a href="'.route('pdf_disposisi', $row->id).'" data-id="'.$row->id.'" class="btn btn-info btn-sm"><i class="fa fa-file-pdf"></i></a>
                ';
                return $action;
            })
            ->editColumn('tgl_penyelesaian', function ($row) {

                $tgl_kemaren = Carbon::create($row->tgl_penyelesaian)->subDays(1)->format('Y-m-d');

                if (Carbon::now()->format('Y-m-d') == $tgl_kemaren) {
                    $tgl = '<span class="badge badge-warning">'.Carbon::create($row->tgl_penyelesaian)->translatedFormat('d F Y').' </span><br> <b>Besok Tanggal Penyelesaian</b>';

                } else {
                    $tgl = Carbon::create($row->tgl_penyelesaian)->translatedFormat('d F Y');
                }
                return $tgl;
            })
            ->rawColumns(['tgl_penyelesaian', 'action'])
            ->addIndexColumn()
            ->make(true);

    }

    public function index()
    {
        
        return view('disposisi.index');

    }

    public function acc_disposisi($id)
    {
        $acc = Disposisi::where('id', $id)->update([
            'status' => 'Approved '
        ]);

        return back();
    }
}
