<?php

namespace App\Http\Controllers;

use App\Models\JenisSurat;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JenisSuratController extends Controller
{
    public function dataJson()
    {
        return DataTables::of(JenisSurat::orderByDesc('id')->get())
            ->addColumn('action', function ($row) {

                $action = '<a href="javascript:void(0);" 
                data-nama_surat="'.$row->nama_surat.'" 
                data-jenis_surat="'.$row->jenis_surat.'"
                class="btn btn-link btn-detail btn-info shadow btn-sm"> 
                <i class="fa fa-eye"></i></a>

                
                <a href="javascript:void(0);" class="btn btn-link btn-primary btn-md btn-edit" data-id="'.$row->id.'" data-nama_surat="'.$row->nama_surat.'" data-jenis_surat="'.$row->jenis_surat.'"><i class="fa fa-edit"></i></a> 
                
                <a href="javascript:void(0);" data-id="'.$row->id.'" class="btn btn-link btn-danger btn-md btn-delete"><i class="fa fa-times"></i></a>';
                return $action;
            })
            ->make(true);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('jenis_surat.index');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_surat' => 'required',
            'jenis_surat' => 'required',
        ]);

        $req_jenis_surat = $request->all();

        if ($request->id) {

            $jenis_surat = JenisSurat::find($request->id);
            $jenis_surat->update($req_jenis_surat);
            $message = "Data Jenis Surat Berhasil diupdate";


        } else {

            $jenis_surat = JenisSurat::create($req_jenis_surat);
            $message = "Data Jenis Surat Berhasil Disimpan";

        }

        return back()->with('success', $message);
    }

    public function destroy($id)
    {
        $jenis_surat = JenisSurat::find($id);
        $jenis_surat->delete();

        return back();
    }
}
