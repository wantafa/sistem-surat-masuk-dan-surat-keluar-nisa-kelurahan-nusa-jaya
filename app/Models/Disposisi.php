<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Disposisi extends Model
{
    use HasFactory;
    
    protected $table = 'disposisi';
    protected $fillable = [
        'surat_masuk_id',
        'asal',
        'intruksi',
        'diteruskan',
        'tgl_penyelesaian',
        'status',
    ];

    public function surat_masuk()
    {
        return $this->belongsTo(SuratMasuk::class);
    }
}
