<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratMasuk extends Model
{
    use HasFactory;

    protected $table = 'surat_masuk';
    protected $fillable = [
        'jenis_surat_id',
        'disposisi_id',
        'tanggal',
        'no_surat',
        'image',
        'perihal',
        'created_by',
        'updated_by',
    ];

    public function jenis_surat()
    {
        return $this->belongsTo(JenisSurat::class);
    }
    
    public function disposisi()
    {
        return $this->belongsTo(Disposisi::class);
    }


}
