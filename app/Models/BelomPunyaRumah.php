<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BelomPunyaRumah extends Model
{
    use HasFactory;
    protected $table = 'sk_belum_punya_rumah';

}
