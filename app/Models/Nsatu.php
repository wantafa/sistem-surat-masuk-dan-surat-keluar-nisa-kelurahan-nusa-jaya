<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nsatu extends Model
{
    use HasFactory;
    protected $table = 'sk_nikah_n1';

    public function penduduk()
    {
        return $this->hasMany(Penduduk::class);
    }

}
