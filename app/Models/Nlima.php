<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nlima extends Model
{
    use HasFactory;
    protected $table = 'sk_nikah_n5';
}
