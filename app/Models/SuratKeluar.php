<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratKeluar extends Model
{
    use HasFactory;
    protected $table = 'surat_keluar';
    protected $fillable = [
        'tanggal',
        'no_surat',
        'jenis_surat_id',
        'penduduk_id',
        'lampiran',
        'perihal',
        'verification',
        'status',
        'nama_lurah',
        'created_by,'
    ];

    public function jenis_surat()
    {
        return $this->belongsTo(JenisSurat::class);
    }

    public function penduduk()
    {
        return $this->belongsTo(JenisSurat::class);
    }

}
