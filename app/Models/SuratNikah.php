<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratNikah extends Model
{
    use HasFactory;
    protected $table = 'sk_surat_nikah';
    protected $fillable = [
        'penduduk_id',
        'surat_keluar_id',
        'created_by'
    ];

    public function penduduk()
    {
        return $this->hasMany(Penduduk::class);
    }
}
