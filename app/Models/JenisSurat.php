<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisSurat extends Model
{
    use HasFactory;

    protected $table = 'jenis_surat';
    protected $fillable = [
        'nama_surat',
        'jenis_surat',
    ];

    public function surat_masuk()
    {
        return $this->hasMany(SuratMasuk::class);
    }


}
