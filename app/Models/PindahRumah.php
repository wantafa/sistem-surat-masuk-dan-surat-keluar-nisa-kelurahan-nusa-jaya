<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PindahRumah extends Model
{
    use HasFactory;
    protected $table = 'sk_pindah_rumah';

    public function surat_keluar()
    {
        return $this->hasMany(SuratKeluar::class, 'surat_keluar_id');
    }
}
